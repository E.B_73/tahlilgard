module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  important: true,
  theme: {
    // Make tailwind breakpoints same as bootstrap
    screens: {
      sm: '576px',
      md: '768px',
      lg: '992px',
      xl: '1200px',
      xxl: '1600px',
    },
    container: (theme) => ({
      center: true,
      padding: theme('spacing.4'),
    }),
    extend: {
      colors: {
        primary: {
          main: '#f0efef',
          dark: '#003248',
        },
        secondary: {
          main: '#666',
          light: '#808080',
        },
        blue: {
          light: '#78c3e3',
          main: '#0096d6',
        },
        gray: {
          main: '#F0F0F0',
          light: '#F9F9F9',
          transparent: '#5E5E5E44',
        },
      },
    },
  },
  corePlugins: {
    // aspectRatio: false,
    // preflight: false,
  },
  plugins: [
    require('tailwindcss-logical'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/forms'),
  ],
};
