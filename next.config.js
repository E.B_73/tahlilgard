/* eslint-disable */
/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config');
const path = require('path');
const withSass = require('@zeit/next-sass');
module.exports = withSass({
  cssModules: true,
});
module.exports = {
  i18n,
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  images: {
    domains: ['indianewengland.com'],
  },
  async redirects() {
    return [
      {
        source: '/courses',
        destination: '/explore',
        permanent: true,
      },
    ];
  },
};
