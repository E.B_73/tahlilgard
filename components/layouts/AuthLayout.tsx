import { ReactElement, ReactNode } from 'react';
import Header from '@/components/header/Header';

interface AuthLayoutProps {
  children?: ReactNode;
}

const AuthLayout = ({ children }: AuthLayoutProps) => {
  return (
    <>
      <Header />
      <main className="container flex items-center justify-center min-h-[calc(100vh_-_80px)]">
        {children}
      </main>
    </>
  );
};
export const getLayout = (page: ReactElement) => {
  return <AuthLayout>{page}</AuthLayout>;
};

export default AuthLayout;
