import { ReactElement, ReactNode } from 'react';
import Header from '@/components/header/Header';
import SortBar from '@/components/layouts/SortBar';
import Footer from '@/components/footer/Footer';
import LearningFilter from '@/components/filters/LearningFilter';
import Search from '@/components/filters/Search';
import { useTranslation } from 'next-i18next';
import SortForm from '@/components/filters/SortForm';
import SortFormModal from '@/components/filters/SortFormModal';

interface LearningLayoutProps {
  children?: ReactNode;
}

const LearningLayout = ({ children }: LearningLayoutProps) => {
  const { t } = useTranslation('common', { keyPrefix: 'sortbar' });
  const sortOptions = t('learning', { returnObjects: true });
  return (
    <div className="relative">
      <Header />
      <Search />
      <div className="container relative flex bg-white">
        <aside className="hidden w-60 lg:block">
          <LearningFilter />
        </aside>
        <div className="flex-grow">
          <SortBar
            sortForm={<SortForm options={sortOptions} />}
            sortFormModal={<SortFormModal options={sortOptions} />}
            filterModalContent={<LearningFilter />}
          />
          <main className="w-full">{children}</main>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export const getLayout = (page: ReactElement) => {
  return <LearningLayout>{page}</LearningLayout>;
};
export default LearningLayout;
