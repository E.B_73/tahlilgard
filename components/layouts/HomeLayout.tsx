import { ReactElement, ReactNode } from 'react';
import Header from '@/components/header/Header';
import Footer from '@/components/footer/Footer';
import CartModal from '@/components/cart/CartModal';

interface HomeLayoutProps {
  children?: ReactNode;
}

const HomeLayout = ({ children }: HomeLayoutProps) => {
  return (
    <div className="relative">
      <Header />
      <main className="w-full">{children}</main>
      <Footer />
      <CartModal />
    </div>
  );
};

export const getLayout = (page: ReactElement) => {
  return <HomeLayout>{page}</HomeLayout>;
};
export default HomeLayout;
