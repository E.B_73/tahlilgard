import { useTranslation } from 'next-i18next';
import { BsSortDown } from 'react-icons/bs';
import { FiFilter } from 'react-icons/fi';
import { Button } from 'antd';
import { ReactNode, useState } from 'react';
import CustomModal from '@/components/global/CustomModal';

interface SortBarProps {
  filterModalContent: ReactNode;
  sortForm: ReactNode;
  sortFormModal: ReactNode;
}

const SortBar = ({
  sortForm,
  sortFormModal,
  filterModalContent,
}: SortBarProps) => {
  const [openFilter, setOpenFilter] = useState(false);
  const [openSort, setOpenSort] = useState(false);
  const { t } = useTranslation('common');
  return (
    <div className="p-2 bg-white border-b ">
      {sortForm}
      <div className="flex gap-2 lg:hidden">
        <Button
          type="default"
          size="middle"
          className="!flex !items-center rounded-lg !gap-2 text-black"
          icon={<FiFilter />}
          onClick={() => setOpenFilter(true)}
        >
          {t('sortbar.filter_button')}
        </Button>
        <Button
          type="default"
          size="middle"
          className="!flex !items-center rounded-lg !gap-2 text-black"
          onClick={() => setOpenSort(true)}
          icon={<BsSortDown />}
        >
          {t('sortbar.sort_button')}
        </Button>
      </div>
      <CustomModal
        title={t('sort_modal_title')}
        open={openSort}
        setOpen={setOpenSort}
        footer={null}
      >
        {sortFormModal}
      </CustomModal>
      <CustomModal
        title={t('filter_modal_title')}
        open={openFilter}
        setOpen={setOpenFilter}
        cancelText={t('filter_cancel_text')}
        okText={t('filter_ok_text')}
      >
        {filterModalContent}
      </CustomModal>
    </div>
  );
};

export default SortBar;
