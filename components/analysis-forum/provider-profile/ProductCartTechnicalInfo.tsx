import { Rate } from 'antd';
import { FiArrowDownLeft, FiArrowUpRight } from 'react-icons/fi';

interface ProductCartTechnicalInfoProps {
  providerName?: string;
  providerAvatar?: string;
  providerRank?: number;
  providerOrder?: 'short' | 'long';
}
const ProductCartTechnicalInfo = ({
  providerName,
  providerOrder,
  providerAvatar,
  providerRank = 0,
}: ProductCartTechnicalInfoProps) => {
  return (
    <div className="flex items-center justify-between gap-2 p-2">
      <div className="flex items-center gap-2">
        <div className="w-10 h-10 overflow-hidden border-2 border-white rounded-full shadow-xl">
          <img
            alt="user-image"
            src={providerAvatar ?? '/media/icons/avatar.png'}
            className="max-w-full max-h-full"
          />
        </div>
        <div className="flex flex-col items-start justify-between">
          <h2 className="text-sm text-secondary-main">{providerName}</h2>
          <Rate disabled defaultValue={providerRank} className="text-xs" />
        </div>
      </div>
      {providerOrder === 'long' ? (
        <FiArrowUpRight className="text-green-500" size={25} />
      ) : (
        <FiArrowDownLeft className="text-red-500" size={25} />
      )}
    </div>
  );
};

export default ProductCartTechnicalInfo;
