import { Button, Rate } from 'antd';
import Image from 'next/image';
import clsx from 'clsx';
import { useTranslation } from 'next-i18next';

const Biography = () => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  return (
    <div className="flex flex-wrap items-center gap-4 md:gap-5">
      <div className="relative w-32 h-32 border-2 border-white rounded-full shadow-lg md:w-40 md:h-40">
        <Image
          className="relative max-w-full max-h-full"
          layout="fill"
          objectFit="fill"
          src={'/media/icons/avatar.png'}
        />
      </div>
      <div className="flex flex-col gap-2">
        <div className="flex items-center gap-1">
          <h2 className="text-sm font-bold text-secondary-main">{t('id')}</h2>
          <h2>ابراهیم</h2>
        </div>
        <div className="flex items-center gap-1">
          <h2 className="text-sm font-bold text-secondary-main">{t('rate')}</h2>
          <Rate disabled defaultValue={2} className="text-base" />
        </div>
        <div className="mt-3">
          <Button className={clsx('rounded-md w-32')}>{t('follow')}</Button>
        </div>
      </div>
      <div className="flex-grow w-60">
        <p className="w-full leading-normal text-secondary-light line-clamp-5">
          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
          از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و
          سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
          متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه
          درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با
          نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان
          خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
        </p>
      </div>
    </div>
  );
};

export default Biography;
