import { LoadingOutlined } from '@ant-design/icons';

const LoadingProfile = () => {
  return (
    <div className="flex items-center justify-center w-screen h-screen bg-white">
      <LoadingOutlined className="!text-blue-main !text-8xl" />
    </div>
  );
};

export default LoadingProfile;
