import { useTranslation } from 'next-i18next';
import { FormattedNumber } from 'react-intl';

const ProviderUsualInfo = () => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  return (
    <div className="h-full py-4 lg:px-4 lg:py-0">
      <h2 className="mb-3 font-bold">{t('statistics')}</h2>
      <div className="flex flex-col justify-start h-full gap-2">
        <span className="flex items-center justify-between gap-2">
          <h2>{t('response_time')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('number_of_learning_packages')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('number_of_webinar')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('number_of_live_trade')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('follower')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('following')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
      </div>
    </div>
  );
};

export default ProviderUsualInfo;
