import { Button } from 'antd';
import {
  ShoppingCartOutlined,
  // HeartOutlined,
} from '@ant-design/icons';
import { FaAngleLeft } from 'react-icons/fa';
import { FormattedNumber } from 'react-intl';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import Image from 'next/image';
import ProductCartTechnicalInfo from '@/components/analysis-forum/provider-profile/ProductCartTechnicalInfo';

export interface ProductCardProps {
  title: string;
  imgUrl: string;
  link: string;
  price?: number;
  technical?: boolean;
  providerName?: string;
  providerAvatar?: string;
  providerRank?: number;
  providerOrder?: 'short' | 'long';
}
const ProductCard = ({
  title,
  imgUrl,
  link,
  price = 0,
  technical = false,
  providerAvatar,
  providerName,
  providerRank,
  providerOrder,
}: ProductCardProps) => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  return (
    <ul className="w-56 space-y-2 overflow-hidden bg-white rounded-md">
      <div className="relative w-full h-36">
        <Image
          className="relative max-w-full max-h-full"
          layout="fill"
          src={imgUrl}
          objectFit="cover"
        />
      </div>
      <h2 className="w-full p-2 font-light min-h-28 line-clamp-2">{title}</h2>
      {technical && (
        <ProductCartTechnicalInfo
          providerName={providerName}
          providerOrder={providerOrder}
          providerAvatar={providerAvatar}
          providerRank={providerRank}
        />
      )}
      <div className="flex items-center justify-between h-10 gap-2 bg-primary-main">
        {price ? (
          <h2 className="flex gap-1 font-bold mis-1">
            <FormattedNumber value={price} />
            <span>{t('toman')}</span>
          </h2>
        ) : (
          price === 0 && <h2 className="mis-1">{t('free')}</h2>
        )}

        <Link href={link}>
          <Button
            icon={
              price ? (
                <ShoppingCartOutlined />
              ) : (
                price === 0 && <FaAngleLeft className="icon" size={20} />
              )
            }
            className="flex items-center justify-center w-12 h-10 text-white bg-blue-light rounded-be-md rounded-is-none"
          />
        </Link>
      </div>
    </ul>
  );
};

export default ProductCard;
