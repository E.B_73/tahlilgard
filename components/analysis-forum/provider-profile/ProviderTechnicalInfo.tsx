import { FormattedNumber } from 'react-intl';
import { useTranslation } from 'next-i18next';

const ProviderTechnicalInfo = () => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  return (
    <div className="h-full py-4 lg:px-4 lg:py-0">
      <h2 className="mb-3 font-bold">{t('indicators')}</h2>
      <div className="flex flex-col justify-start h-full gap-2">
        <span className="flex items-center justify-between gap-2">
          <h2>{t('WR')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('ROI')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
        <span className="flex items-center justify-between gap-2">
          <h2>{t('RR')}</h2>
          <h2>
            <FormattedNumber value={0} />
          </h2>
        </span>
      </div>
    </div>
  );
};

export default ProviderTechnicalInfo;
