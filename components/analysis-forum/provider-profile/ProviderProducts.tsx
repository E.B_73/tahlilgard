import ProductsCategory from '@/components/analysis-forum/provider-profile/ProductsCategory';
import ProductsList from '@/components/analysis-forum/provider-profile/ProductsList';
import Collapse from '@/components/global/Collapse';
import { useState } from 'react';

const list = [
  { title: 'آمورش', imgUrl: '/media/images/banner.jpg', link: '/' },
  { title: 'آمورش', imgUrl: '/media/images/banner.jpg', link: '/' },
  { title: 'آمورش', imgUrl: '/media/images/banner.jpg', link: '/' },
];
const ProviderProducts = () => {
  const [category, setCategory] = useState<string>('0');
  return (
    <div>
      <ProductsCategory category={category} setCategory={setCategory} />
      <Collapse show={category === '0'}>
        <ProductsList list={list} />
      </Collapse>
      <Collapse show={category === '1'}>
        <ProductsList list={list} />
      </Collapse>
      <Collapse show={category === '2'}>
        <ProductsList list={list} />
      </Collapse>
    </div>
  );
};

export default ProviderProducts;
