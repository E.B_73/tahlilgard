import Biography from '@/components/analysis-forum/provider-profile/Biography';
import ProviderProducts from '@/components/analysis-forum/provider-profile/ProviderProducts';
import StatisticalInfo from '@/components/analysis-forum/provider-profile/StatisticalInfo';

const ProviderAllInfo = () => {
  return (
    <div className="space-y-10 bg-white">
      <Biography />
      <StatisticalInfo />
      <ProviderProducts />
    </div>
  );
};

export default ProviderAllInfo;
