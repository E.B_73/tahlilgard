import CategoryButton from '@/components/global/Radio';
import { ChangeEvent, Dispatch, SetStateAction } from 'react';
import { useTranslation } from 'next-i18next';

interface ProductsCategoryProps {
  setCategory: Dispatch<SetStateAction<string>>;
  category: string;
}
const ProductsCategory = ({ category, setCategory }: ProductsCategoryProps) => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  const selectCategoryHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setCategory(e.target.value);
  };
  return (
    <div className="flex items-center justify-center w-full gap-2 px-3 py-4 rounded-lg bg-primary-main">
      <CategoryButton
        className="category"
        labelClassName="px-2 py-1 text-xs font-bold text-black bg-white rounded-lg lg:px-3 lg:py-2 hover:shadow-md"
        value="0"
        id="0"
        name="category"
        checked={category === '0'}
        onChange={selectCategoryHandler}
        label={t('learning_packages')}
      />
      <CategoryButton
        className="category"
        labelClassName="px-2 py-1 text-xs font-bold text-black bg-white rounded-lg lg:px-3 lg:py-2 hover:shadow-md"
        value="1"
        id="1"
        name="category"
        checked={category === '1'}
        onChange={selectCategoryHandler}
        label={t('live_trade')}
      />
      <CategoryButton
        className="category"
        labelClassName="px-2 py-1 text-xs font-bold text-black bg-white rounded-lg lg:px-3 lg:py-2 hover:shadow-md"
        value="2"
        id="2"
        name="category"
        checked={category === '2'}
        onChange={selectCategoryHandler}
        label={t('webinar')}
      />
    </div>
  );
};

export default ProductsCategory;
