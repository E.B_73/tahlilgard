import { Pie, PieConfig, measureTextWidth } from '@ant-design/plots';
import { View } from '@antv/g2';
import clsx from 'clsx';
import { useTranslation } from 'next-i18next';

interface CircleChartProps {
  title: string;
  data: { type: string; value: number }[];
  className?: string;
}

function renderStatistic(
  containerWidth: number,
  text: string,
  style: React.CSSProperties,
) {
  const { width: textWidth, height: textHeight } = measureTextWidth(
    text,
    style,
  );
  const R = containerWidth / 2; // r^2 = (w / 2)^2 + (h - offsetY)^2

  let scale = 1;

  if (containerWidth < textWidth) {
    scale = Math.min(
      Math.sqrt(
        Math.abs(
          Math.pow(R, 2) /
            (Math.pow(textWidth / 2, 2) + Math.pow(textHeight, 2)),
        ),
      ),
      1,
    );
  }

  const textStyleStr = `width:${containerWidth}px;`;
  return `<div style="${textStyleStr};font-size:${
    style.fontSize
  }px;font-weight:500;height:20px;display:flex;justify-content:center;line-height:${
    scale < 1 ? 1 : 'inherit'
  };">${text}</div>`;
}

const CircleChart = ({ title, className, data }: CircleChartProps) => {
  const { t } = useTranslation('common');
  const config: PieConfig = {
    padding: 0,
    data,
    angleField: 'value',
    colorField: 'type',
    radius: 1,
    innerRadius: 0.6,
    legend: false,
    meta: {
      value: {
        formatter: (v: any) => `${v}`,
      },
    },
    label: {
      type: 'inner',
      offset: '-50%',
      autoRotate: false,
      content: '{value}',
      style: {
        textAlign: 'center',
        fontFamily: 'Vazir',
      },
    },
    statistic: {
      title: {
        offsetY: -5,
        customHtml: (
          container: HTMLElement,
          _view: View,
          datum?: Record<string, any>,
        ) => {
          const { width, height } = container.getBoundingClientRect();
          const d = Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height / 2, 2));
          const text = datum ? datum.type : t('total');
          return renderStatistic(d, text, {
            fontSize: 13,
          });
        },
      },
      content: {
        offsetY: 4,
        style: {
          fontSize: '32px',
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        customHtml: (
          container: HTMLElement,
          _view: View,
          datum?: Record<string, any>,
          data?: Record<string, any>[],
        ) => {
          const { width } = container.getBoundingClientRect();
          const text = datum
            ? `${datum.value}`
            : `${data?.reduce((r: any, d: any) => r + d.value, 0)}`;
          return renderStatistic(width, text, {
            fontSize: 25,
          });
        },
      },
    },
    interactions: [
      {
        type: 'element-active',
      },
      {
        type: 'pie-statistic-active',
      },
    ],
  };
  return (
    <div
      className={clsx(
        'flex flex-col items-center justify-center flex-1',
        className,
      )}
    >
      <Pie {...config} width={220} height={220} />
      <h2 className="text-lg font-semibold text-secondary-main">{title}</h2>
    </div>
  );
};

export default CircleChart;
