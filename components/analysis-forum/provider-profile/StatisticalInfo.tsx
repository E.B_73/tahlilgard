import CircleChart from '@/components/analysis-forum/provider-profile/CircleChart';
import ProviderTechnicalInfo from '@/components/analysis-forum/provider-profile/ProviderTechnicalInfo';
import ProviderUsualInfo from '@/components/analysis-forum/provider-profile/ProviderUsualInfo';
import { useTranslation } from 'next-i18next';

const data = [
  {
    type: 'موفق',
    value: 25,
  },
  {
    type: 'ناموفق',
    value: 25,
  },
  {
    type: 'جاری',
    value: 50,
  },
];

const StatisticalInfo = () => {
  const { t } = useTranslation('analysis', { keyPrefix: 'provider_profile' });
  return (
    <div className="grid divide-y lg:divide-y-0 lg:divide-x lg:divide-x-reverse lg:grid-cols-3">
      <CircleChart
        title={t('chart_title')}
        data={data}
        className="py-4 lg:px-4 lg:py-0"
      />
      <ProviderUsualInfo />
      <ProviderTechnicalInfo />
    </div>
  );
};

export default StatisticalInfo;
