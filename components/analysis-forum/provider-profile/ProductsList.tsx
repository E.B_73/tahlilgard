import ProductCard, {
  ProductCardProps,
} from '@/components/analysis-forum/provider-profile/ProductCard';

interface ProductsListProps {
  list: ProductCardProps[];
}
const ProductsList = ({ list }: ProductsListProps) => {
  return (
    <div className="flex flex-wrap items-center justify-center gap-2 mt-4 md:justify-start">
      {list.map((item, i) => (
        <ProductCard
          key={i}
          title={item.title}
          imgUrl={item.imgUrl}
          link={item.link}
        />
      ))}
    </div>
  );
};

export default ProductsList;
