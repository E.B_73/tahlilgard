import { AutoComplete, Input } from 'antd';
import React from 'react';

const options = [
  {
    label: 'Libraries',
    value: 'Libraries',
  },
  {
    label: 'Solutions',
    value: 'Libraries',
  },
  {
    label: 'Articles',
    value: 'Libraries',
  },
];

const SearchPackages = () => {
  return (
    <div className="flex items-center justify-center w-full my-4">
      <AutoComplete
        className="w-full mx-auto md:max-w-md search-Packages rounded-3xl"
        options={options}
      >
        <Input.Search placeholder="نام نماد یا عنوان تحلیل" />
      </AutoComplete>
    </div>
  );
};

export default SearchPackages;
