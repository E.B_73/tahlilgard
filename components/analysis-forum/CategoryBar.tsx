import Radio from '@/components/global/Radio';
import useAppStore from '@/store/store';
import { CardTypeEnum } from '@/utils/enums';
import { useTranslation } from 'next-i18next';

const CategoryBar = () => {
  const { t } = useTranslation('analysis');
  const cardType = useAppStore((state) => state.cardType);
  const setCardType = useAppStore((state) => state.setCardType);
  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCardType(e.target.value as CardType);
  };

  return (
    <div className="flex items-center justify-center w-full gap-4 my-6">
      <Radio
        id="a1"
        name="analysis-forum"
        label={t('analysis')}
        className="category"
        labelClassName="px-2 py-1 text-xs font-bold text-black bg-gray-main rounded-lg lg:px-3 lg:py-2 hover:shadow-md"
        value="analysis"
        checked={cardType === CardTypeEnum.analysis}
        onChange={changeHandler}
      />
      <Radio
        id="a2"
        name="analysis-forum"
        label={t('analysts')}
        className="category"
        labelClassName="px-2 py-1 text-xs font-bold text-black bg-gray-main rounded-lg lg:px-3 lg:py-2 hover:shadow-md"
        value="provider"
        checked={cardType === CardTypeEnum.provider}
        onChange={changeHandler}
      />
    </div>
  );
};

export default CategoryBar;
