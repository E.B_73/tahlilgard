//React
import { FC, useState, useEffect } from 'react';
//components
import { Input, AutoComplete } from 'antd';
//styles
import classes from '@/styles/sass/analysis-forum.module.scss';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
const SearchBar: FC = () => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const currencies = [
    'BTC',
    'ETH',
    'Bnb',
    'TRX',
    'fdf',
    'ksl',
    'fsfs',
    'fpf',
    'plkk',
    'fdas',
    'qad',
    'fdd',
    'BTC',
    'ETH',
    'Bnb',
    'TRX',
    'fdf',
    'ksl',
    'fsfs',
    'fpf',
    'plkk',
    'fdas',
    'qad',
    'fdd',
    'ETH',
    'Bnb',
    'TRX',
    'fdf',
    'ksl',
    'fsfs',
    'fpf',
    'plkk',
    'fdas',
    'qad',
    'fdd',
  ];
  const [showingCurr, setShowingCurr] = useState<string[]>([]);
  const [filterWord, setFilterWord] = useState<string>('');
  const autoCompleteProps = {
    options: showingCurr.map((item) => {
      return { label: item, value: item };
    }),
    autoFocus: false,
  };
  useEffect(() => {
    setShowingCurr(
      currencies.filter((coin) =>
        coin.toLowerCase().trim().includes(filterWord.toLowerCase().trim()),
      ),
    );
  }, [filterWord]);
  return (
    <>
      <div className={classes.searchContainer}>
        <AutoComplete
          {...autoCompleteProps}
          style={{ width: '100%' }}
          dropdownClassName={'bg-dark'}
          // defaultOpen={true}
        >
          <Input.Search
            suffix={<span></span>}
            key={'input'}
            allowClear
            enterButton
            value={filterWord}
            size="large"
            className={`custom rounded ${
              router.locale === 'fa' ? 'rtl' : 'ltr'
            }`}
            placeholder={t('Enter The Market Item')}
            onChange={(e) => {
              setFilterWord(e.target.value);
            }}
          />
        </AutoComplete>
      </div>
      {/* <Carousel {...sliderProps}>
				{showingCurr.map((e) => (
					<div
						key={e.toString()}
						className={
							e !== "BTC"
								? classes.searchItem
								: classes.selectedSearchItem
						}
					>
						{e}
					</div>
				))}
			</Carousel> */}
    </>
  );
};

export default SearchBar;
