import paths from '@/utils/paths';
import { Divider, Rate } from 'antd';
import Image from 'next/image';
import Link from 'next/link';

interface ProviderCardProps {
  avatar?: string;
  name: string;
  link: string;
}

const ProviderCard = ({ avatar, name, link }: ProviderCardProps) => {
  return (
    <Link href={link}>
      <div className="p-2 rounded-lg min-w-[210px] w-56 justify-self-center bg-gray-main overflow-hidden shadow-lg cursor-pointer">
        <div className="flex justify-between gap-4">
          <div className="flex flex-col items-center gap-1">
            <div className="relative border border-white rounded-full shadow-md w-14 h-14">
              <Image
                className="relative max-w-full max-h-full"
                layout="fill"
                objectFit="fill"
                src={avatar ?? '/media/icons/avatar.png'}
              />
            </div>
            <h2 className="text-sm">{name}</h2>
            <Rate disabled count={4} className="!text-sm" value={2} />
          </div>
          <div className="text-xs">
            <div className="flex items-center justify-between gap-3">
              <h2 className="font-bold">فالور</h2>
              <h2 className="text-[10px] font-bold text-secondary-main">۳۶۵</h2>
            </div>
            <div className="flex items-center justify-between gap-3">
              <h2 className="font-bold">درصد موفقیت</h2>
              <h2 className="text-[10px] font-bold text-secondary-main">۹۸٪</h2>
            </div>
            <div className="flex items-center justify-between gap-3">
              <h2 className="font-bold">R/R</h2>
              <h2 className="text-[10px] font-bold text-secondary-main">۲</h2>
            </div>
            <div className="flex items-center justify-between gap-3">
              <h2 className="font-bold">ROI</h2>
              <h2 className="text-[10px] font-bold text-secondary-main">۱۳۵</h2>
            </div>
          </div>
        </div>
        <Divider className="!my-2 !text-xs" orientation="right">
          بیوگرافی
        </Divider>
        <p className="my-2 text-sm">
          تتیب یباستایسب سیباسیب مایبسیسبت سیباسیبل سیبابیل
        </p>
        <Link href={paths.providerId('2')}>
          <button className="w-full p-2 mt-2 text-sm transition-all duration-200 bg-white border rounded-md hover:border-blue-main">
            پروفایل تحلیلگرد
          </button>
        </Link>
      </div>
    </Link>
  );
};

export default ProviderCard;
