//React
import React, { FC, useState, useEffect } from 'react';
//antd
import { Col, Row } from 'antd';
//styles
import classes from '@/styles/sass/analysis-forum.module.scss';
//hooks
import { useTranslation } from 'next-i18next';
//components
import Countdown from 'react-countdown';

interface CardProps {
  userImage?: string;
  type: 'buy' | 'sell';
  logo?: string;
  expire?: Date | number | string | null;
  user?: string;
  currency?: string;
  children?: string;
  hasBuy?: boolean;
}
const Card: FC<CardProps> = ({
  userImage,
  type,
  logo,
  expire,
  user,
  currency,
  children,
  hasBuy,
}) => {
  const { t } = useTranslation('common');
  const [expireState, setExpire] = useState<string | Date | number | null>('');
  useEffect(() => {
    setExpire(Date.now() > expire! ? null : expire!);
  }, []);
  return (
    <div className={classes.card}>
      <Row justify="center" align="middle" gutter={8}>
        <Col span={8}>
          <img
            width="68rem"
            height="68rem"
            src={logo ?? '/unknown-curr.png'}
            className="rounded"
          />
        </Col>
        <Col span={8}>
          <img
            width="68rem"
            height="68rem"
            src={`/${type}.png`}
            className="rounded"
          />
        </Col>
        <Col span={8}>
          <img
            width="68rem"
            height="68rem"
            src={userImage ?? '/avatar.jpg'}
            className="rounded"
          />
        </Col>
      </Row>
      <Row justify="center" align="middle" style={{ marginTop: '0.5em' }}>
        <Col span={8}>{currency?.toUpperCase() ?? 'Unknown'}</Col>
        <Col span={8}>{type}</Col>
        <Col span={8}>{user ?? 'User'}</Col>
      </Row>
      <Row justify="center" align="middle" style={{ margin: '1em 0' }}>
        <Col span={7} className={classes.subCard} style={{ fontSize: '0.6em' }}>
          {expireState ? (
            <Countdown
              date={expireState}
              onComplete={() => {
                setExpire(null);
              }}
            />
          ) : (
            'Expired'
          )}
        </Col>
        <Col span={7} className={classes.subCard}>
          ff
        </Col>
        <Col span={7} className={classes.subCard}>
          ff
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
          ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
          lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
          ipsum
        </Col>
      </Row>
      <Row justify="center" align="middle">
        <Col
          span={8}
          className={!hasBuy ? classes.subCardSuccess : classes.subCard}
          style={{ marginTop: '0.5em' }}
        >
          {hasBuy ? t('Show') : t('Buy')}
        </Col>
      </Row>
    </div>
  );
};

export default Card;
