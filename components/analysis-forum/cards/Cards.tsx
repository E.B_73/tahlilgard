import React, { FC } from 'react';
import AnalysisFilter from '@/components/filters/AnalysisFilter';
import SortBar from '@/components/layouts/SortBar';
import AnalysisCard from '@/components/analysis-forum/cards/AnalysisCard';
import ProviderCard from '@/components/analysis-forum/cards/ProviderCard';
import useAppStore from '@/store/store';
import ProviderFilter from '@/components/filters/ProviderFilter';
import SortForm from '@/components/filters/SortForm';
import { useTranslation } from 'next-i18next';
import SortFormModal from '@/components/filters/SortFormModal';
import { Pagination } from 'antd';
import { CardTypeEnum } from '@/utils/enums';

const analysisFakeData = [
  {
    price: 290000,
    link: '/',
    providerName: 'علیرضا',
    currencyName: 'بیتکوین',
    currencyLogo:
      'https://indianewengland.com/wp-content/uploads/2018/11/bitcoin.png',
    description: 'سلام و بلمنتملخحیبل ابینایلحجبلا البا',
    expireDate: new Date('2023-06-25'),
    viewerCount: 345,
  },
  {
    price: 290000,
    link: '/',
    providerName: 'علیرضا',
    currencyName: 'بیتکوین',
    currencyLogo:
      'https://indianewengland.com/wp-content/uploads/2018/11/bitcoin.png',
    description: 'سلام و بلمنتملخحیبل ابینایلحجبلا البا',
    expireDate: new Date('2023-06-25'),
    viewerCount: 345,
  },
  {
    price: 290000,
    link: '/',
    providerName: 'علیرضا',
    currencyName: 'بیتکوین',
    currencyLogo:
      'https://indianewengland.com/wp-content/uploads/2018/11/bitcoin.png',
    description: 'سلام و بلمنتملخحیبل ابینایلحجبلا البا',
    expireDate: new Date('2023-06-25'),
    viewerCount: 345,
  },
  {
    price: 290000,
    link: '/',
    providerName: 'علیرضا',
    currencyName: 'بیتکوین',
    currencyLogo:
      'https://indianewengland.com/wp-content/uploads/2018/11/bitcoin.png',
    description: 'سلام و بلمنتملخحیبل ابینایلحجبلا البا',
    expireDate: new Date('2023-06-25'),
    viewerCount: 345,
  },
];
const providerFakeData = [
  {
    name: 'ابراهیم',
    link: '/',
  },
  {
    name: 'ابراهیم',
    link: '/',
  },
  {
    name: 'ابراهیم',
    link: '/',
  },
];
const Cards: FC = () => {
  const cardType = useAppStore((state) => state.cardType);
  const { t } = useTranslation('common', { keyPrefix: 'sortbar' });
  const providerSortOptions = t('provider', { returnObjects: true });
  const analysisSortOptions = t('analysis', { returnObjects: true });
  const analysisMode = cardType === CardTypeEnum.analysis;
  return (
    <div className="flex w-full bg-white">
      <aside className="hidden bg-white w-60 lg:block">
        {analysisMode ? <AnalysisFilter /> : <ProviderFilter />}
      </aside>
      <div className="flex-1">
        <SortBar
          sortForm={
            <SortForm
              options={analysisMode ? analysisSortOptions : providerSortOptions}
            />
          }
          sortFormModal={
            <SortFormModal
              options={analysisMode ? analysisSortOptions : providerSortOptions}
            />
          }
          filterModalContent={
            analysisMode ? <AnalysisFilter /> : <ProviderFilter />
          }
        />
        <div className="grid gap-4 my-4 bg-white place-content-center md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          {analysisMode
            ? analysisFakeData.map((item, i) => (
                <AnalysisCard key={i} {...item} />
              ))
            : providerFakeData.map((item, i) => (
                <ProviderCard key={i} {...item} />
              ))}
        </div>
        <div className="flex items-center justify-center w-full md:justify-end">
          <Pagination responsive />
        </div>
      </div>
    </div>
  );
};

export default Cards;
