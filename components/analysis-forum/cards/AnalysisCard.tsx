import { Button, Divider, Rate } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import {
  EyeInvisibleOutlined,
  ShoppingCartOutlined,
  LeftOutlined,
} from '@ant-design/icons';
import { FormattedDate, FormattedNumber } from 'react-intl';
import { useTranslation } from 'next-i18next';

interface AnalysisCardProps {
  price?: number;
  link: string;
  providerName: string;
  providerAvatar?: string;
  currencyLogo: string;
  currencyName: string;
  expireDate: Date;
  viewerCount: number;
  description: string;
}

const AnalysisCard = ({
  price,
  link,
  providerAvatar,
  providerName,
  currencyLogo,
  currencyName,
  expireDate,
  viewerCount,
  description,
}: AnalysisCardProps) => {
  const { t } = useTranslation('common');
  return (
    <div className="rounded-lg min-w-[210px] w-56 bg-gray-main overflow-hidden justify-self-center shadow-lg">
      <div className="p-2">
        <div className="flex items-start justify-between gap-4">
          <div className="flex flex-col items-center gap-1">
            <div className="relative border border-white rounded-full shadow-md w-14 h-14">
              <Image
                className="relative max-w-full max-h-full"
                layout="fill"
                objectFit="fill"
                src={providerAvatar ?? '/media/icons/avatar.png'}
              />
            </div>
            <h2 className="text-sm">{providerName}</h2>
            <Rate disabled count={4} className="!text-sm" value={2} />
          </div>
          <div className="flex flex-col items-center gap-1">
            <div className="relative w-10 h-10 border border-white rounded-full shadow-md">
              <Image
                className="relative max-w-full max-h-full"
                layout="fill"
                objectFit="fill"
                src={currencyLogo}
              />
            </div>
            <h2 className="text-xs">{currencyName}</h2>
          </div>
        </div>
        <Divider className="!my-2" />
        <div className="grid grid-cols-2 mb-2 text-xs">
          <div className="flex flex-col items-center justify-self-center">
            <h2 className="font-bold">تاریخ انقضا</h2>
            <h2 className="font-bold text-[10px] text-secondary-main">
              <FormattedDate value={expireDate} />
            </h2>
          </div>
          <div className="flex flex-col items-center justify-self-center">
            <h2 className="font-bold">تعداد خریداری شده</h2>
            <h2 className="font-bold text-[10px] text-secondary-main">
              <FormattedNumber value={viewerCount} useGrouping={false} />
            </h2>
          </div>
        </div>
        <div className="relative my-2">
          <p className="text-justify blur-sm">{description}</p>
          <span className="absolute top-0 flex items-center justify-center w-full h-full">
            <EyeInvisibleOutlined className="text-2xl" />
          </span>
        </div>
      </div>
      <div className="flex items-center justify-between h-10 gap-2 bg-neutral-100">
        {price ? (
          <h2 className="flex gap-1 text-sm font-bold mis-1">
            <FormattedNumber value={price} />
            <span>{t('toman')}</span>
          </h2>
        ) : (
          price === 0 && t('free')
        )}

        <Link href={link}>
          <Button
            icon={
              price ? (
                <ShoppingCartOutlined />
              ) : (
                price === 0 && <LeftOutlined className="!text-sm icon" />
              )
            }
            className="!bg-blue-300 !rounded-be-lg !h-full !w-12 !text-white !rounded-is-none"
          />
        </Link>
      </div>
    </div>
  );
};

export default AnalysisCard;
