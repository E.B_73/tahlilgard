import { Select } from 'antd';

const ColorSelect = () => {
  return (
    <Select defaultValue="1" options={[{ value: '1', label: 'انتخاب کنید' }]} />
  );
};

export default ColorSelect;
