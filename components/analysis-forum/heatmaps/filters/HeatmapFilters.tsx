import CategorySelect from '@/components/analysis-forum/heatmaps/filters/CategorySelect';
import ColorSelect from '@/components/analysis-forum/heatmaps/filters/ColorSelect';
import FinanceSelect from '@/components/analysis-forum/heatmaps/filters/FinanceSelect';
import IndustrySelect from '@/components/analysis-forum/heatmaps/filters/IndustrySelect';
import MarketSelect from '@/components/analysis-forum/heatmaps/filters/MarketSelect';
import SizeSelect from '@/components/analysis-forum/heatmaps/filters/SizeSelect';
import { Button, Form } from 'antd';
import { useTranslation } from 'next-i18next';

const HeatmapFilters = () => {
  const { t } = useTranslation('analysis', { keyPrefix: 'heatmap_filters' });
  return (
    <Form
      layout="horizontal"
      className="flex flex-col items-center w-full gap-4 p-4 mt-8 mb-4 rounded-lg bg-gray-main"
    >
      <div className="flex flex-col gap-y-4 lg:divide-x-2 lg:divide-x-reverse lg:flex-row">
        <div className="space-y-4 pie-0 lg:pie-4">
          <h2 className="text-sm font-bold text-center">{t('map_filter')}</h2>
          <div className="flex flex-wrap gap-4">
            <Form.Item label={t('finance')} name="finance" className="!mb-0">
              <FinanceSelect />
            </Form.Item>
            <Form.Item label={t('market')} name="market" className="!mb-0">
              <MarketSelect />
            </Form.Item>
            <Form.Item label={t('industry')} name="industry" className="!mb-0">
              <IndustrySelect />
            </Form.Item>
          </div>
        </div>
        <div className="space-y-4 pis-0 lg:pis-4">
          <h2 className="text-sm font-bold text-center">{t('map_setting')}</h2>
          <div className="flex flex-wrap gap-4">
            <Form.Item label={t('category')} name="category" className="!mb-0">
              <CategorySelect />
            </Form.Item>
            <Form.Item label={t('size')} name="size" className="!mb-0">
              <SizeSelect />
            </Form.Item>
            <Form.Item label={t('color')} name="color" className="!mb-0">
              <ColorSelect />
            </Form.Item>
          </div>
        </div>
      </div>
      <Button
        htmlType="submit"
        className="w-20 h-10 text-sm font-bold text-white rounded-lg bg-blue-main"
      >
        {t('action')}
      </Button>
    </Form>
  );
};

export default HeatmapFilters;
