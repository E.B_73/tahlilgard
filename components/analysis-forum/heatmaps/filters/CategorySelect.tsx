import { Select } from 'antd';

const CategorySelect = () => {
  return (
    <Select defaultValue="1" options={[{ value: '1', label: 'انتخاب کنید' }]} />
  );
};

export default CategorySelect;
