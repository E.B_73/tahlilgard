type marketType = 'bourse' | 'faraBourse' | 'crypto' | 'forex';
type cryptoSizeType = 'volume' | 'marketCap';
type bourseSizeType = 'value' | 'volume' | 'sameWeight';
type ColorType = 'daily' | 'weekly' | 'monthly' | 'yearly';
