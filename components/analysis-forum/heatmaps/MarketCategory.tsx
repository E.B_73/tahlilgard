import Radio from '@/components/global/Radio';
import { useTranslation } from 'next-i18next';

const MarketCategory = () => {
  const { t } = useTranslation('common');

  return (
    <div className="flex items-center justify-center w-full gap-4 my-4">
      <Radio
        id="m0"
        name="market"
        className="market"
        value={0}
        label={t('Bourse')}
        imgSrc="/media/images/bourse.png"
        defaultChecked={true}
      />
      <Radio
        id="m1"
        name="market"
        className="market"
        value={1}
        label={t('FaraBourse')}
        imgSrc="/media/images/farabourse.png"
      />
      <Radio
        id="m2"
        name="market"
        className="market"
        value={2}
        label={t('Cryptocurrency')}
        imgSrc="/media/images/cryptocurrency.png"
      />
      <Radio
        id="m3"
        name="market"
        className="market"
        value={3}
        label={t('Forex')}
        imgSrc="/media/images/forex.png"
      />
    </div>
  );
};

export default MarketCategory;
