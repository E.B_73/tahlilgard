// react
import React, { useEffect, useState } from 'react';
// components
import { Chart } from 'react-google-charts';
// types
import './types.d.ts';
import { parentCreator } from '@/utils/heatmap/parentCreator';
import { useMutation } from '@apollo/client';
import { GET_BOURSE_HEATMAP, GET_CRYPTO_HEATMAP } from '@/graphql/mutations';
import MarketCategory from '@/components/analysis-forum/heatmaps/MarketCategory';
import HeatmapFilters from '@/components/analysis-forum/heatmaps/filters/HeatmapFilters';
import { LoadingOutlined } from '@ant-design/icons';
import { useTranslation } from 'next-i18next';

export default function HeatMap() {
  const { t } = useTranslation('analysis', { keyPrefix: 'heatmap_filters' });
  const [heatmapData, setHeatmapData] = useState<(string | number | null)[][]>([
    [],
  ]);
  const [market] = useState<marketType>('bourse');

  const [
    getBourseHeatmap,
    { data: bourseHeatmapData, loading: loadingBourse, error: bourseError },
  ] = useMutation(GET_BOURSE_HEATMAP, { fetchPolicy: 'network-only' });
  const [
    getCryptoHeatmap,
    { data: cryptoHeatmapData, loading: loadingCrypto, error: cryptoError },
  ] = useMutation(GET_CRYPTO_HEATMAP, { fetchPolicy: 'network-only' });

  const heatmapSwitch = {
    bourse: getBourseHeatmap,
    crypto: getCryptoHeatmap,
    // forex: () => {},
    // faraBourse: () => {},
  };

  useEffect(() => {
    console.log('im in useEffect');
    if (bourseHeatmapData?.getBoorsHeatmap?.heatmap) {
      const heatData = parentCreator(bourseHeatmapData.getBoorsHeatmap.heatmap);
      setHeatmapData(heatData);
    }
  }, [bourseHeatmapData]);
  useEffect(() => {
    if (cryptoHeatmapData?.getCryptoHeatmap?.heatmap) {
      const heatData = parentCreator(
        cryptoHeatmapData.getCryptoHeatmap.heatmap,
      );
      setHeatmapData(heatData);
      console.log('im in useEffect', heatData);
    }
  }, [cryptoHeatmapData]);
  useEffect(() => {
    console.log('market changed , ', market);
    const sendMarketType = async (market: marketType) => {
      await heatmapSwitch[market as keyof typeof heatmapSwitch]();
    };
    sendMarketType(market);
  }, [market]);

  return (
    <>
      <MarketCategory />
      <HeatmapFilters />
      <div className="flex items-center justify-center p-2 rounded-md bg-primary-main">
        {loadingBourse || loadingCrypto ? (
          <LoadingOutlined className="text-4xl text-blue-main" />
        ) : cryptoError || bourseError ? (
          <h3 className="text-sm font-bold">{t('error')}</h3>
        ) : (
          <Chart
            className="shadow"
            width={'100%'}
            height={'85vh'}
            chartType="TreeMap"
            loader={<LoadingOutlined className="text-4xl text-blue-main" />}
            data={heatmapData}
            options={{
              showScale: true,
              allowHtml: true,
              fontColor: '#fff',
              minColor: '#d62d4d',
              midColor: '#4e4752',
              maxColor: '#02ad65',
              fontFamily: 'Vazir',
              fontSize: 20,
              headerHeight: 42,
              maxDepth: 2,
              headerColor: '#003248',
              maxPostDepth: 1,
              headerHighlightColor: '#fff',
              generateTooltip: (_row: any, _size: any, value: string) => {
                return (
                  '<div style="background:#fd9; padding:10px; border-style:solid"> ' +
                  value +
                  '</div>'
                );
              },
            }}
          />
        )}
      </div>
    </>
  );
}
