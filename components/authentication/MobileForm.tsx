import { Button, Form, Input } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useTranslation } from 'next-i18next';
import { MOBILE_REGEX } from '@/utils/constants';
import useSendCheckAuthMutation from '@/hooks/mutations/useSendCheckAuthMutation';
import { checkPhoneNumber } from '@/utils/helper';

const MobileForm = () => {
  const { t } = useTranslation();
  const { sendCheckAuth, loading } = useSendCheckAuthMutation();
  const submitHandler = async (values: { phone: string }) => {
    if (values.phone) {
      await sendCheckAuth({
        variables: {
          userCheckAuthInput: {
            phoneNumber: checkPhoneNumber(values.phone),
          },
        },
      });
    }
  };
  return (
    <div className="flex flex-col gap-4">
      <h2 className="text-xl font-semibold">{t('Login to Tahlilgard')}</h2>
      <Form onFinish={submitHandler} layout="horizontal">
        <Form.Item
          style={{ marginBottom: '24px' }}
          name="phone"
          rules={[
            { message: t('Please input your phone!'), required: true },
            {
              validator: (_, value) => {
                if (!MOBILE_REGEX.test(value)) {
                  return Promise.reject(t('Please Enter a valid Phone'));
                }
                return Promise.resolve();
              },
            },
          ]}
        >
          <Input
            placeholder={t('Phone Number')}
            type="number"
            className="!h-10 !rounded-lg ltr placeholder:!text-right"
          />
        </Form.Item>
        <Form.Item>
          <div className="flex flex-col gap-2">
            <Button
              className="!h-10 !rounded-lg !w-full"
              disabled={loading}
              type="primary"
              htmlType="submit"
            >
              {loading ? <LoadingOutlined /> : t('Login / Signup')}
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default MobileForm;
