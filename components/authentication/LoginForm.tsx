import { Button, Form, Input } from 'antd';
import { LoadingOutlined, UndoOutlined } from '@ant-design/icons';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import useSendLoginMutation from '@/hooks/mutations/useSendLoginMutation';
import { useEffect } from 'react';
import useSendForgetPassMutation from '@/hooks/mutations/useSendForgetPassMutation';
import LoadingWithBackdrop from '@/components/ui/LoadingWithBackdrop';

const LoginForm = () => {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { setStep, recordId, phoneNumber } = useAppStore(
    (state) => ({
      setStep: state.setStep,
      recordId: state.recordId,
      phoneNumber: state.phoneNumber,
    }),
    shallow,
  );
  const { sendLogin, loading } = useSendLoginMutation();
  const { sendPass, loading: passLoading } = useSendForgetPassMutation();
  // fill phone input with mobile number from Previous step
  useEffect(() => {
    form.setFieldValue('phoneNumber', phoneNumber);
  }, [phoneNumber]);
  // submit form
  const submitHandler = async (values: { password: string }) => {
    if (values.password) {
      await sendLogin({
        variables: {
          userLoginInput: {
            recordId,
            password: values.password,
          },
        },
      });
    }
  };
  const sendForgetPassHandler = async () => {
    await sendPass({
      variables: {
        forgetPasswordInput: {
          recordId,
        },
      },
    });
  };

  return (
    <div className="relative flex flex-col gap-4">
      <h2 className="text-xl font-semibold">{t('Login to Tahlilgard')}</h2>
      <Form
        form={form}
        onFinish={submitHandler}
        layout="vertical"
        wrapperCol={{ span: 24 }}
      >
        <Form.Item
          style={{ marginBottom: '24px' }}
          name="phoneNumber"
          className="!p-0"
        >
          <Input
            suffix={
              <UndoOutlined
                className="!text-lg !text-[#a6a6a6] !border-is-2 !pis-1 !pie-[11px]"
                onClick={() => setStep('enter_mobile')}
              />
            }
            placeholder={t('Phone Number')}
            name="phoneNumber"
            className="!h-10 !rounded-lg !p-0 !pis-[11px] ltr-input"
            disabled
          />
        </Form.Item>
        <Form.Item
          style={{ marginBottom: '24px' }}
          name="password"
          rules={[
            { required: true, message: t('Please input your password!') },
          ]}
        >
          <Input.Password
            className="!h-10 !rounded-lg"
            placeholder={t('Password')}
          />
        </Form.Item>
        <Form.Item
          style={{ marginBottom: '24px' }}
          wrapperCol={{ md: { span: 24 } }}
        >
          <Button
            disabled={loading}
            type="primary"
            htmlType="submit"
            className="!h-10 !rounded-lg !w-full"
          >
            {loading ? <LoadingOutlined /> : t('Login')}
          </Button>
        </Form.Item>
      </Form>
      <div>
        {t('Forget your password?')}
        <a onClick={sendForgetPassHandler}>{t('Recovery password')}</a>
      </div>
      {passLoading && <LoadingWithBackdrop />}
    </div>
  );
};

export default LoginForm;
