import { Row, Col, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import useDetectUser from '@/hooks/global/useDetectUser';
import useAppStore from '@/store/store';
import MobileForm from '@/components/authentication/MobileForm';
import RegisterForm from '@/components/authentication/RegisterForm';
import LoginForm from '@/components/authentication/LoginForm';
import SendCodeForm from '@/components/authentication/SendCodeForm';
import Collapse from '@/components/global/Collapse';
import { AuthStpEnum } from '@/utils/enums';

const { Title, Text } = Typography;
const Container = () => {
  const { loading } = useDetectUser(true);
  const step = useAppStore((state) => state.step);
  const { t } = useTranslation('common');

  return (
    <Row
      justify="center"
      align="middle"
      className="rounded-3xl !bg-primary-main"
    >
      {loading ? (
        <div>loading</div>
      ) : (
        <>
          <Col md={12} sm={24} className="!p-8 w-full">
            <Collapse show={step === AuthStpEnum.ENTER_MOBILE}>
              <MobileForm />
            </Collapse>
            <Collapse show={step === AuthStpEnum.LOGIN}>
              <LoginForm />
            </Collapse>
            <Collapse show={step === AuthStpEnum.REGISTER}>
              <RegisterForm />
            </Collapse>
            <Collapse show={step === AuthStpEnum.SEND_CODE}>
              <SendCodeForm />
            </Collapse>
          </Col>
          <Col
            md={12}
            sm={24}
            className="!p-8 md:border-is-2 border-bs-2 md:border-bs-0"
          >
            <Title level={5}>
              {t('You must have account to use services')}
            </Title>
            <Text>{t('Signup is free')}</Text>
            <Text>{t('You can use of Analysis after signup')}</Text>
          </Col>
        </>
      )}
    </Row>
  );
};

export default Container;
