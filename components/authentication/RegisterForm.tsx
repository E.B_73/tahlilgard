import Timer from '@/components/global/Timer';
import { Button, Form, Input, Switch } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import { useState } from 'react';
import useSendRegisterMutation from '@/hooks/mutations/useSendRegisterMutation';
import useSendResendCodeMutation from '@/hooks/mutations/useSendResendCodeMutation';
// zustand store
import useAppStore from '@/store/store';

const RegisterForm = () => {
  const { t } = useTranslation();
  const [showTimer, setShowTimer] = useState<boolean>(true);
  const recordId = useAppStore((state) => state.recordId);
  const countDown = useAppStore((state) => state.countDown);
  const { sendRegister, loading } = useSendRegisterMutation();
  const { sendResendCode, loading: reSendLoading } =
    useSendResendCodeMutation('auth_form');
  // resend code
  const reSendHandler = async () => {
    await sendResendCode({
      variables: {
        reSendVerifyCodeInput: {
          recordId,
        },
      },
    });
    setShowTimer(true);
  };

  const submitHandler = async (values: {
    isProvider: boolean;
    name: string;
    username: string;
    password: string;
    code: string;
  }) => {
    if (!_.isEmpty(values)) {
      sendRegister({
        variables: {
          userSignupInput: {
            name: values.name,
            username: values.username,
            password: values.password,
            isProvider: values.isProvider,
            verificationCode: values.code,
            recordId,
          },
        },
      });
    }
  };
  return (
    <div className="flex flex-col gap-4">
      <h2 className="mb-6 text-xl font-semibold">{t('Signup')}</h2>
      <Form
        wrapperCol={{ span: 24 }}
        layout="vertical"
        onFinish={submitHandler}
        initialValues={{ isProvider: false }}
      >
        <div className="!flex !justify-center !gap-4">
          <span>{t("I'm a buyer of analysis")}</span>
          <Form.Item className="mb-6" name="isProvider">
            <Switch className="!bg-blue-main !w-10" />
          </Form.Item>
          <span> {t("I'm Analyzer")}</span>
        </div>
        <Input.Group
          compact
          className="!flex !items-center !justify-center mb-6"
        >
          <Form.Item
            name="code"
            className="!m-0 !w-3/5 md:!w-30"
            normalize={(e) => {
              if (/^\d{1,5}$/g.test(e)) return e;
              else if (/^\d{6}$/g.test(e)) return e.slice(0, 5);
            }}
          >
            <Input
              className="!h-10 ltr !rounded-ie-lg placeholder:!text-3xl !text-xl placeholder:!text-center placeholder:!-translate-y-2 !text-center !font-bold"
              type="number"
              maxLength={5}
              placeholder="_ _ _ _ _ "
            />
          </Form.Item>
          {showTimer ? (
            <div className="bg-white !w-2/5 md:!w-20 !h-10  !rounded-ie-lg border-[1px] !flex !justify-center !items-center">
              <Timer duration={countDown} completeHandler={setShowTimer} />
            </div>
          ) : (
            <Button
              disabled={reSendLoading}
              type="default"
              size="middle"
              onClick={reSendHandler}
              className="!w-2/5 md:!w-25 !h-10 !rounded-ie-lg"
            >
              {reSendLoading ? <LoadingOutlined /> : t('Resend Code')}
            </Button>
          )}
        </Input.Group>
        <Form.Item
          className="mb-6"
          name="name"
          rules={[
            {
              type: 'string',
              whitespace: true,
            },
            {
              required: true,
              message: t('Please input your name!'),
            },
          ]}
        >
          <Input className="!h-10 !rounded-lg" placeholder={t('First name')} />
        </Form.Item>
        <Form.Item
          className="mb-6"
          name="username"
          rules={[
            {
              type: 'string',
              whitespace: true,
            },
            {
              required: true,
              message: t('Please input your username!'),
            },
          ]}
        >
          <Input className="!h-10 !rounded-lg" placeholder={t('Username')} />
        </Form.Item>
        <Form.Item
          className="mb-6"
          name="password"
          rules={[
            {
              required: true,
              message: t('Please input your password!'),
            },
          ]}
        >
          <Input.Password
            className="!h-10 !rounded-lg"
            placeholder={t('Password')}
          />
        </Form.Item>
        <Form.Item
          className="mb-6"
          name="confirmPassword"
          dependencies={['password']}
          rules={[
            {
              required: true,
              message: t('Please confirm your password!'),
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error(t('Passwords doesnt Equal')));
              },
            }),
          ]}
        >
          <Input.Password
            className="!h-10 !rounded-lg"
            placeholder={t('Confirm Password')}
          />
        </Form.Item>

        <Form.Item wrapperCol={{ md: { span: 24 } }}>
          <Button
            disabled={loading}
            type="primary"
            htmlType="submit"
            className="!h-10 !rounded-lg !w-full"
          >
            {loading ? <LoadingOutlined /> : t('Signup')}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default RegisterForm;
