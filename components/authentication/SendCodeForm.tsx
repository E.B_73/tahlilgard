import { Button, Form, Input } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { useTranslation } from 'next-i18next';
import Timer from '@/components/global/Timer';
import { useState } from 'react';
import useSendSecondLoginMutation from '@/hooks/mutations/useSendSecondLoginMutation';
import useSendResendCodeMutation from '@/hooks/mutations/useSendResendCodeMutation';
// store
import useAppStore from '@/store/store';

const SendCodeForm = () => {
  const { t } = useTranslation();
  const [showTimer, setShowTimer] = useState<boolean>(true);
  const recordId = useAppStore((state) => state.recordId);
  const countDown = useAppStore((state) => state.countDown);
  const { sendCode, loading } = useSendSecondLoginMutation();
  const { sendResendCode, loading: reSendLoading } =
    useSendResendCodeMutation('auth_form');
  // resend code
  const reSendHandler = async () => {
    await sendResendCode({
      variables: {
        reSendVerifyCodeInput: {
          recordId,
        },
      },
    });
    setShowTimer(true);
  };

  const submitHandler = async (values: { code: string }) => {
    if (values.code) {
      await sendCode({
        variables: {
          userSecondLoginInput: {
            recordId,
            code: values.code,
          },
        },
      });
    }
  };
  return (
    <div className="flex flex-col gap-4">
      <h2 className="mb-6 text-xl font-semibold">{t('Enter Code')}</h2>
      <Form
        onFinish={submitHandler}
        wrapperCol={{ span: 24 }}
        layout="vertical"
      >
        <div className="!gap-8 mb-4">
          <Input.Group compact className="!flex !items-center !justify-center">
            <Form.Item
              name="code"
              className="!m-0 !w-3/5 md:!w-30"
              normalize={(e) => {
                if (/^\d{1,5}$/g.test(e)) return e;
                else if (/^\d{6}$/g.test(e)) return e.slice(0, 5);
              }}
            >
              <Input
                className="!h-10 !rounded-ie-lg ltr placeholder:!text-3xl !text-xl placeholder:!text-center placeholder:!-translate-y-2 !text-center !font-bold"
                type="number"
                maxLength={5}
                placeholder="_ _ _ _ _"
              />
            </Form.Item>
            {showTimer ? (
              <div className="bg-white !w-2/5 md:!w-20 !h-10  !rounded-ie-lg border-[1px] !flex !justify-center !items-center">
                <Timer duration={countDown} completeHandler={setShowTimer} />
              </div>
            ) : (
              <Button
                disabled={reSendLoading}
                type="default"
                size="middle"
                onClick={reSendHandler}
                className="!w-2/5 md:!w-25 !h-10 !rounded-ie-lg"
              >
                {reSendLoading ? <LoadingOutlined /> : t('Resend Code')}
              </Button>
            )}
          </Input.Group>
        </div>
        <Form.Item wrapperCol={{ md: { span: 24 } }}>
          <Button
            disabled={loading}
            type="primary"
            htmlType="submit"
            className="!h-10 !rounded-lg !w-full"
          >
            {loading ? <LoadingOutlined /> : t('Login')}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SendCodeForm;
