import { Select, Collapse, CollapsePanelProps } from 'antd';
import { useTranslation } from 'next-i18next';
const { Panel } = Collapse;

const ProviderRank = (props: CollapsePanelProps) => {
  const { t } = useTranslation('common', {
    keyPrefix: 'sidebar',
  });
  const rankOptions = t('provider_rank.options', { returnObjects: true });

  return (
    <Panel className="w-full" {...props}>
      <Select
        className="w-full"
        options={rankOptions}
        placeholder={t('provider_rank.placeholder')}
      />
    </Panel>
  );
};

export default ProviderRank;
