import { useLiveTradeCategoryOptions } from '@/hooks/global/useFilter';
import { Checkbox, Collapse, CollapsePanelProps } from 'antd';
const { Panel } = Collapse;

interface LiveTradeCategoryProps extends CollapsePanelProps {
  searchTxt?: string;
}

const LiveTradeCategory = (props: LiveTradeCategoryProps) => {
  const checkboxOptions = useLiveTradeCategoryOptions();
  return (
    <Panel {...props}>
      <Checkbox.Group
        className="flex flex-col gap-1 mt-3"
        options={checkboxOptions}
      />
    </Panel>
  );
};

export default LiveTradeCategory;
