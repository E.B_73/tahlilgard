import { Select, Collapse, CollapsePanelProps } from 'antd';
import { useTranslation } from 'next-i18next';
const { Panel } = Collapse;

const AnalysisActive = (props: CollapsePanelProps) => {
  const { t } = useTranslation('common', {
    keyPrefix: 'sidebar',
  });
  const activeOptions = t('analysis_active.options', { returnObjects: true });

  return (
    <Panel className="w-full" {...props}>
      <Select
        className="w-full"
        options={activeOptions}
        placeholder={t('analysis_active.placeholder')}
      />
    </Panel>
  );
};

export default AnalysisActive;
