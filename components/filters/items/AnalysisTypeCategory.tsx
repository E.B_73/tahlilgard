import { Collapse, CollapsePanelProps, Input, Checkbox } from 'antd';
import { ChangeEvent, useState } from 'react';
import { useTranslation } from 'next-i18next';
const { Panel } = Collapse;

const checkboxOptions = [
  { label: 'تکنیکال', value: '1' },
  { label: 'فاندامنتال', value: '2' },
  { label: 'اخباری', value: '3' },
];

const AnalysisTypeCategory = (props: CollapsePanelProps) => {
  const { t } = useTranslation('common', { keyPrefix: 'sidebar' });
  const [searchOptions, setSearchOptions] = useState(checkboxOptions);
  const searchHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchOptions(
      checkboxOptions.filter((item) => item.label.includes(e.target.value)),
    );
  };

  return (
    <Panel {...props}>
      <Input
        className="w-full h-10 rounded-md"
        placeholder={t('search')}
        onChange={searchHandler}
      />
      <Checkbox.Group
        className="flex flex-col gap-1 mt-3"
        options={searchOptions}
        // onChange={onChange}
      />
    </Panel>
  );
};

export default AnalysisTypeCategory;
