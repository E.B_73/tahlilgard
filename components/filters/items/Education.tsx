import { AutoComplete, Collapse, CollapsePanelProps } from 'antd';
const { Panel } = Collapse;
const options = [{ value: 'مورد اول' }, { value: 'مورد دوم' }];

interface EducationProps extends CollapsePanelProps {
  searchTxt?: string;
}
const Education = (props: EducationProps) => {
  return (
    <Panel {...props}>
      <AutoComplete
        options={options}
        className="w-full"
        // onSelect={onSelect}
        // onSearch={onSearch}
        placeholder={props.searchTxt}
      />
    </Panel>
  );
};

export default Education;
