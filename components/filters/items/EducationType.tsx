import { useEducationTypeOptions } from '@/hooks/global/useFilter';
import { Checkbox, Collapse, CollapsePanelProps } from 'antd';
const { Panel } = Collapse;

const EducationType = (props: CollapsePanelProps) => {
  const checkboxOptions = useEducationTypeOptions();
  return (
    <Panel {...props}>
      <Checkbox.Group
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: '2px',
          marginTop: '5px',
        }}
        options={checkboxOptions}
        // onChange={onChange}
      />
    </Panel>
  );
};

export default EducationType;
