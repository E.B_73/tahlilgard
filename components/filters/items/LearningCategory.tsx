import { useLearningCategoryOptions } from '@/hooks/global/useFilter';
import { AutoComplete, Checkbox, Collapse, CollapsePanelProps } from 'antd';
const { Panel } = Collapse;

interface LearningCategoryProps extends CollapsePanelProps {
  searchTxt?: string;
}

const LearningCategory = (props: LearningCategoryProps) => {
  const checkboxOptions = useLearningCategoryOptions();
  return (
    <Panel {...props}>
      <AutoComplete
        options={checkboxOptions}
        className="w-full"
        // onSelect={onSelect}
        // onSearch={onSearch}
        placeholder={props.searchTxt}
      />
      <Checkbox.Group
        className="flex flex-col gap-1 mt-3"
        options={checkboxOptions}
        // onChange={onChange}
      />
    </Panel>
  );
};

export default LearningCategory;
