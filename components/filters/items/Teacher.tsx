import { AutoComplete, Collapse, CollapsePanelProps } from 'antd';

const { Panel } = Collapse;

interface TeacherProps extends CollapsePanelProps {
  searchTxt?: string;
}

const Teacher = (props: TeacherProps) => {
  return (
    <Panel {...props}>
      <AutoComplete
        className="w-full"
        // onSelect={onSelect}
        // onSearch={onSearch}
        placeholder={props.searchTxt}
      />
    </Panel>
  );
};

export default Teacher;
