import { usePriceOptions } from '@/hooks/global/useFilter';
import { Checkbox, Collapse, CollapsePanelProps } from 'antd';
const { Panel } = Collapse;

const Price = (props: CollapsePanelProps) => {
  const checkboxOptions = usePriceOptions();
  return (
    <Panel {...props}>
      <Checkbox.Group
        className="flex flex-col gap-1 mt-3"
        options={checkboxOptions}
        defaultValue={['مورد اول']}
        // onChange={onChange}
      />
    </Panel>
  );
};

export default Price;
