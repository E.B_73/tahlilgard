import LiveTradeCategory from '@/components/filters/items/LiveTradeCategory';
import Teacher from '@/components/filters/items/Teacher';
import { Collapse } from 'antd';
import { useTranslation } from 'next-i18next';

const LiveTradeFilter = () => {
  const { t } = useTranslation('liveTrade', { keyPrefix: 'sidebar' });
  return (
    <div className="sticky w-full top-20">
      <Collapse
        defaultActiveKey={['0']}
        expandIconPosition="end"
        bordered={false}
        className="md:!pie-5 !bg-white"
      >
        <LiveTradeCategory
          key="0"
          header={t('category.title')}
          className="py-2"
        />
        <Teacher
          key="1"
          header={t('analyzer_name.title')}
          className="py-2"
          searchTxt={t('analyzer_name.search')}
        />
      </Collapse>
    </div>
  );
};

export default LiveTradeFilter;
