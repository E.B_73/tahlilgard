import Radio from '@/components/global/Radio';
import { useTranslation } from 'next-i18next';
import { BsSortDown } from 'react-icons/bs';

interface SortFormProps {
  options: { label: string; value: string }[];
}

const SortForm = ({ options }: SortFormProps) => {
  const { t } = useTranslation('common', { keyPrefix: 'sortbar' });
  return (
    <div className="items-center hidden gap-4 lg:flex">
      <div className="flex items-center gap-2">
        <BsSortDown />
        <h2>{t('title')}</h2>
      </div>
      <div className="flex gap-3">
        {options.map((item, i) => (
          <Radio
            key={i}
            id={`s${i}`}
            name="sort"
            className="sort"
            value={item.value}
            label={item.label}
          />
        ))}
      </div>
    </div>
  );
};

export default SortForm;
