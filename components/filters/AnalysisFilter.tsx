import { Collapse } from 'antd';
import { useTranslation } from 'next-i18next';
import AnalysisActive from '@/components/filters/items/AnalysisActive';
import AnalysisCategory from '@/components/filters/items/AnalysisCategory';
import AnalysisSpecialCategory from '@/components/filters/items/AnalysisSpecialCategory';

const AnalysisFilter = () => {
  const { t } = useTranslation('common', { keyPrefix: 'sidebar' });
  return (
    <div className="sticky top-20">
      <Collapse
        defaultActiveKey={['0']}
        expandIconPosition="end"
        bordered={false}
        className="md:!pie-5 !bg-white"
      >
        <AnalysisActive
          key="0"
          header={t('analysis_active.title')}
          className="py-2"
        />
        <AnalysisCategory key="1" header={t('analysis_category_title')} />
        <AnalysisSpecialCategory
          key="2"
          header={t('analysis_special_category_title')}
        />
      </Collapse>
    </div>
  );
};

export default AnalysisFilter;
