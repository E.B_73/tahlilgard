import { Collapse } from 'antd';
import { useTranslation } from 'next-i18next';
import ProviderRank from '@/components/filters/items/ProviderRank';
import MarketCategory from '@/components/filters/items/MarketCategory';
import AnalysisTypeCategory from '@/components/filters/items/AnalysisTypeCategory';

const ProviderFilter = () => {
  const { t } = useTranslation('common', { keyPrefix: 'sidebar' });
  return (
    <div className="sticky top-20">
      <Collapse
        defaultActiveKey={['0']}
        expandIconPosition="end"
        bordered={false}
        className="md:!pie-5 !bg-white"
      >
        <ProviderRank
          key="0"
          header={t('provider_rank.title')}
          className="py-2"
        />
        <AnalysisTypeCategory
          key="2"
          header={t('analysis_type_category_title')}
        />
        <MarketCategory key="1" header={t('market_category_title')} />
      </Collapse>
    </div>
  );
};

export default ProviderFilter;
