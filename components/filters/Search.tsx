import { AutoComplete, Input } from 'antd';
import { useState } from 'react';
import { useTranslation } from 'next-i18next';

const Search = () => {
  const { t } = useTranslation('learning');
  const [options, setOptions] = useState<{ label: string; value: string }[]>(
    [],
  );
  const searchHandler = (value: string) => {
    let res: { value: string; label: string }[] = [];
    if (!value || value.length < 4) {
      res = [];
    } else {
      // fetch list of items
      // res = ['gmail.com', '163.com', 'qq.com'].map((domain) => ({
      //   value,
      //   label: `${value}@${domain}`,
      // }));
      res = [
        { label: 'در حال دریافت موارد پیشنهادی از سرور ...', value: 'item' },
      ];
    }
    setOptions(res);
  };
  return (
    <div className="flex justify-center w-full my-2">
      <AutoComplete
        onSearch={searchHandler}
        options={options}
        className="w-full !mx-2 md:w-[500px] auto-complete"
      >
        <Input.Search placeholder={t('learning_search')} size="large" />
      </AutoComplete>
    </div>
  );
};

export default Search;
