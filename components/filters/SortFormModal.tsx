import { Radio, Space } from 'antd';

interface SortFormModalProps {
  options: { label: string; value: string }[];
}

const SortFormModal = ({ options }: SortFormModalProps) => {
  return (
    <div>
      <Radio.Group defaultValue={options[0].value}>
        <Space direction="vertical">
          {options.map((item, i) => (
            <Radio key={i} value={item.value}>
              {item.label}
            </Radio>
          ))}
        </Space>
      </Radio.Group>
    </div>
  );
};

export default SortFormModal;
