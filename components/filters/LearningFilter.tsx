import LearningCategory from '@/components/filters/items/LearningCategory';
import EducationType from '@/components/filters/items/EducationType';
import Price from '@/components/filters/items/Price';
import Teacher from '@/components/filters/items/Teacher';
import { Collapse } from 'antd';
import { useTranslation } from 'next-i18next';

const LearningFilter = () => {
  const { t } = useTranslation('common', { keyPrefix: 'sidebar' });
  return (
    <div className="sticky top-20">
      <Collapse
        defaultActiveKey={['0']}
        expandIconPosition="end"
        bordered={false}
        className="md:!pie-5 !bg-white"
      >
        <LearningCategory
          key="0"
          header={t('category.title')}
          className="py-2"
          searchTxt={t('category.search')}
        />
        <Price header={t('price.title')} className="py-2" key="2" />
        <EducationType
          key="3"
          header={t('type_of_education.title')}
          className="py-2"
        />
        <Teacher
          key="4"
          header={t('teacher_name.title')}
          className="py-2"
          searchTxt={t('teacher_name.search')}
        />
      </Collapse>
    </div>
  );
};

export default LearningFilter;
