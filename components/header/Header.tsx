import clsx from 'clsx';
import { CloseOutlined, MenuOutlined } from '@ant-design/icons';
import { HiOutlineLogin, HiOutlineShoppingBag } from 'react-icons/hi';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import useHeaderMenuItems from '@/hooks/global/useHeaderMenuItems';
import HeaderItem from '@/components/header/HeaderItem';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import { useRouter } from 'next/router';
import { Badge, Button } from 'antd';
import paths from '@/utils/paths';
import { shallow } from 'zustand/shallow';

const Header2 = () => {
  const { t } = useTranslation('common');
  const { push, pathname } = useRouter();
  const { token, logout, setShowCart } = useAppStore(
    (state) => ({
      token: state.token,
      logout: state.logout,
      setShowCart: state.setShowCart,
    }),
    shallow,
  );
  const [showList, setShowList] = useState(false);
  const menuItem = useHeaderMenuItems();

  const loginClickHandler = () => {
    token ? logout() : push(paths.check_auth);
  };
  const hideListHandler = () => {
    setShowList((prevState) => !prevState);
  };
  const showCartHandler = () => {
    token ? setShowCart(true) : push(paths.check_auth);
  };

  return (
    <header className="sticky top-0 z-50 w-full bg-primary-dark">
      <nav className="container grid grid-cols-3 md:grid-cols-12">
        <div
          className="flex items-center order-last h-20 gap-1 cursor-pointer md:order-first cols-span-1 justify-self-center md:col-span-3 md:justify-self-start group"
          onClick={() => push(paths.home)}
        >
          <div className="relative w-10 h-14">
            <Image
              src="/media/icons/headerLogo2.png"
              alt="logo"
              layout="fill"
              objectFit="contain"
              className="relative w-full transition-all group-hover:-translate-y-1"
            />
          </div>
          <div className="relative h-20 w-28">
            <Image
              src="/media/icons/headerLogo.png"
              alt="logo"
              layout="fill"
              objectFit="contain"
              className="relative transition-all max-full group-hover:translate-y-1"
            />
          </div>
        </div>
        <ul className="items-center hidden gap-5 my-0 md:col-span-6 md:flex md:justify-self-center lg:justify-self-center md:text-xs lg:text-sm">
          {menuItem.map((item, i) => (
            <HeaderItem
              key={i}
              title={item.title}
              link={item.link}
              iconName={item.icon}
              active={item.link === pathname}
              className="self-center !text-white"
            />
          ))}
        </ul>
        <div className="flex items-center order-last col-span-1 gap-3 justify-self-end md:col-span-3">
          {token && (
            <Link
              href={process.env.NEXT_PUBLIC_PANEL! + `?token=${token}`}
              replace={false}
            >
              <a>
                <Button
                  type="primary"
                  className="items-center justify-center hidden p-2 text-xs font-bold border-0 rounded-full shadow-xl mie-4 bg-blue-main md:flex"
                >
                  {t('User panel')}
                </Button>
              </a>
            </Link>
          )}
          <Badge
            count={0}
            size="small"
            className="transition-all cursor-pointer text-gray-main hover:text-blue-main"
          >
            <HiOutlineShoppingBag fontSize={22} onClick={showCartHandler} />
          </Badge>
          <Button
            type="default"
            className="flex gap-1 text-white font-bold bg-transparent group border-0 items-center justify-center p-0 text-xs rounded-full shadow-[12px_76px_98px_-26px] shadow-blue-light"
            onClick={loginClickHandler}
            icon={<HiOutlineLogin className="text-lg text-blue-main" />}
          >
            {token ? t('Logout') : t('Login to Tahlilgard')}
          </Button>
        </div>
        <MenuOutlined
          className="self-center order-first block col-span-1 text-lg text-white justify-self-start md:hidden"
          onClick={hideListHandler}
        />
      </nav>
      {/* mobile list */}
      <div
        className={clsx(
          'fixed z-50 md:hidden w-screen h-screen block-start-0 inline-start-0 bg-[#4f4f4f90]',
          showList ? 'visible' : 'invisible',
        )}
        onClick={hideListHandler}
      ></div>
      <div
        className={clsx(
          ' w-full sm:w-[500px] p-4 h-screen z-[60] flex flex-col fixed bg-white top-0 gap-10 transition-all flx-col md:invisible',
          showList
            ? 'inline-start-0 visible'
            : '-inline-start-full sm:-inline-start-[500px] invisible',
        )}
      >
        {/* close button */}
        <button className="w-[45px] h-[45px] p-2 hover:bg-gray-main rounded-full">
          <CloseOutlined
            className="text-lg font-bold text-gray-700"
            onClick={hideListHandler}
          />
        </button>
        {/* list items */}
        <ul className="flex flex-col gap-8 text-base">
          {menuItem.map((item, i) => (
            <HeaderItem
              key={i}
              title={item.title}
              link={item.link}
              iconName={item.icon}
              active={item.link == pathname}
              className="text-black"
              onClick={hideListHandler}
            />
          ))}
        </ul>
        {token && (
          <Link
            href={process.env.NEXT_PUBLIC_PANEL! + `?token=${token}`}
            replace
          >
            <Button
              type="primary"
              className="flex items-center justify-center p-2 text-xs font-bold border-0 rounded-full shadow-xl bg-blue-main md:hidden"
            >
              {t('User panel')}
            </Button>
          </Link>
        )}
      </div>
    </header>
  );
};

export default Header2;
