import clsx from 'clsx';
import Link from 'next/link';
import { LIProps } from 'react-html-props';
interface HeaderItemProps extends LIProps {
  title: string;
  link: string;
  iconName?: string;
  active?: boolean;
  onClick?: () => void;
}

const HeaderItem = ({ link, title, active, ...rest }: HeaderItemProps) => {
  return (
    <li {...rest}>
      <Link href={link}>
        <a
          className={clsx(
            'flex gap-1 hover:text-blue-light',
            active ? '!text-blue-light' : 'text-black md:text-white',
          )}
        >
          <span>{title}</span>
        </a>
      </Link>
    </li>
  );
};

export default HeaderItem;
