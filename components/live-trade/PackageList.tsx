import PackageCard from '@/components/global/PackageCard';

const list = [
  {
    imgUrl: '/media/images/banner.jpg',
    link: '/live-trade/1',
    title: 'الب',
    price: 6500,
  },
  {
    imgUrl: '/media/images/banner.jpg',
    link: '/live-trade/1',
    title: 'الب',
    price: 6500,
  },
  {
    imgUrl: '/media/images/banner.jpg',
    link: '/live-trade/1',
    title: 'الب',
    price: 6500,
  },
  {
    imgUrl: '/media/images/banner.jpg',
    link: '/live-trade/1',
    title: 'الب',
    price: 6500,
  },
];
const PackageList = () => {
  return (
    <div className="flex flex-wrap gap-4">
      {list.map((item, i) => (
        <PackageCard
          className="max-w-xs"
          key={i}
          title={item.title}
          type="live-trade"
          liveTradeType="live"
          imgUrl={item.imgUrl}
          link={item.link}
          price={item.price}
        />
      ))}
    </div>
  );
};

export default PackageList;
