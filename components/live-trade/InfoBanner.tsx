import { useTranslation } from 'next-i18next';
import { FormattedNumber } from 'react-intl';

const InfoBanner = () => {
  const { t } = useTranslation('liveTrade', { keyPrefix: 'banner' });
  return (
    <div>
      <section className="relative w-full flex items-center md:min-h-[400px] sm:min-h-[50vh] sm:h-max h-[50vh]">
        <div className="absolute left-0 top-0 w-full h-full bg-[url('/media/images/banner.jpg')] bg-no-repeat bg-cover bg-center filter brightness-75" />
        <div className="container relative z-10 flex flex-col items-center justify-center gap-3">
          <p className="text-justify text-white md:text-lg">
            {t('description')}
          </p>
          <div className="p-2 bg-[#0101015f] rounded-xl text-white flex items-center justify-between gap-6">
            <div className="flex flex-col items-center gap-3">
              <h2 className="text-lg text-white">
                <FormattedNumber value={2722}>
                  {(formattedNumber: string) => (
                    <span className="flex gap-1">
                      <span>{formattedNumber}</span>
                      <span>+</span>
                    </span>
                  )}
                </FormattedNumber>
              </h2>
              <h3 className="text-center text-white">{t('title1')}</h3>
            </div>
            <div className="flex flex-col items-center gap-3">
              <h2 className="text-lg text-white">
                <FormattedNumber value={425} />
              </h2>
              <h3 className="text-center text-white">{t('title2')}</h3>
            </div>
            <div className="flex flex-col items-center gap-3">
              <h2 className="text-lg text-white">
                <FormattedNumber value={273}>
                  {(formattedNumber: string) => (
                    <span className="flex gap-1">
                      <span>{formattedNumber}</span>
                      <span>+</span>
                    </span>
                  )}
                </FormattedNumber>
              </h2>
              <h3 className="text-center text-white">{t('title3')}</h3>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default InfoBanner;
