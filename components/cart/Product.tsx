import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { FormattedNumber } from 'react-intl';
import { DeleteOutlined } from '@ant-design/icons';
import clsx from 'clsx';

interface ProductProps {
  imgSrc?: string;
  title: string;
  price: number;
  seller?: string;
  benefits?: string;
  type: 'shopping' | 'modal';
}

const Product = ({
  imgSrc,
  title,
  price,
  seller,
  benefits,
  type,
}: ProductProps) => {
  const { t } = useTranslation('common');
  return (
    <div className="grid w-full grid-cols-12 py-2">
      <button
        className={clsx(
          'flex items-center self-center justify-center col-span-1 text-xs transition-all hover:text-white bg-primary-main hover:bg-red-300',
          type === 'shopping'
            ? 'rounded-full w-7 h-7 md:w-9 md:h-9'
            : 'rounded-sm w-6 h-full',
        )}
      >
        <DeleteOutlined />
      </button>
      <div
        className={clsx(
          'grid col-span-11 gap-2 ',
          type === 'shopping' ? 'md:grid-cols-12' : 'grid-cols-12',
        )}
      >
        <div
          className={clsx(
            'flex items-center gap-3',
            type === 'shopping' ? ' col-span-6' : 'col-span-8',
          )}
        >
          <div className="relative w-20 h-20">
            <Image
              className="relative rounded-lg"
              src={imgSrc ?? '/media/icons/img-placeholder.png'}
              layout="fill"
              objectFit="cover"
              alt="img"
            />
          </div>
          <h3 className="flex-1 max-h-full text-sm font-bold break-words align-middle text-secondary-main line-clamp-3">
            {title}
          </h3>
        </div>
        <div
          className={clsx(
            'self-center justify-between grid-cols-12 col-span-6',
            type === 'shopping' ? 'grid' : 'hidden',
          )}
        >
          <div className="self-center col-span-8 space-y-2">
            <div className="flex items-center gap-2">
              <span className="text-sm text-secondary-light">
                {t('cart.seller')}
              </span>
              <span className="text-sm text-secondary-main">{seller}</span>
            </div>
            <div className="flex items-center gap-2">
              <span className="text-sm text-secondary-light">
                {t('cart.benefits')}
              </span>
              <span className="text-sm break-words align-middle text-secondary-main line-clamp-2">
                {benefits}
              </span>
            </div>
          </div>
          <div className="self-center col-span-4">
            <h3 className="flex flex-col items-center self-center justify-end gap-2 justify-items-end">
              <span className="text-xl font-bold text-secondary-main">
                <FormattedNumber value={price} />
              </span>
              <span className="text-xs text-secondary-light">{t('toman')}</span>
            </h3>
          </div>
        </div>
        <h3
          className={clsx(
            'flex-col items-center self-center justify-end col-span-3 gap-2 justify-items-end',
            type === 'shopping' ? 'hidden' : 'flex',
          )}
        >
          <span className="text-lg font-bold text-secondary-main">
            <FormattedNumber value={price} />
          </span>
          <span className="text-xs text-secondary-light">{t('toman')}</span>
        </h3>
      </div>
    </div>
  );
};

export default Product;
