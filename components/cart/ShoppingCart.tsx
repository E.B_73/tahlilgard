import DiscountForm from '@/components/cart/DiscountForm';
import Product from '@/components/cart/Product';
import SubmitOrderForm from '@/components/cart/SubmitOrderForm';
import { useTranslation } from 'next-i18next';
import { FormattedNumber } from 'react-intl';

const products = [
  {
    title: 'بسته تحلیلی 1',
    price: 450000,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 2',
    price: 26500,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 1',
    price: 450000,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 2',
    price: 26500,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 1',
    price: 450000,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 2',
    price: 26500,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 1',
    price: 450000,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
  {
    title: 'بسته تحلیلی 2',
    price: 26500,
    benefits: 'مزایا',
    seller: 'نام فروشنده',
  },
];

const ShoppingCart = () => {
  const { t } = useTranslation('common');
  return (
    <div className="">
      <section className="my-4 overflow-hidden border rounded-md">
        <header className="grid grid-cols-12 py-4 mx-4 border-b">
          <h2 className="col-span-6 text-lg font-bold md:col-span-9 text-secondary-main justify-self-center">
            {t('cart.products')}
          </h2>
          <h2 className="col-span-6 text-lg font-bold md:col-span-3 text-secondary-main justify-self-end md:justify-self-center">
            {t('cart.price')}
          </h2>
        </header>
        <main className="flex flex-col items-center p-4 divide-y">
          {products.map((item, i) => (
            <Product key={i} {...item} type="shopping" />
          ))}
        </main>
        <footer className="flex flex-col justify-between w-full gap-6 p-4 bg-gray-200 md:flex-row md:items-center">
          <DiscountForm />
          <h3 className="flex items-center justify-center w-full gap-3 md:justify-end">
            <span className="text-lg text-gray-700 ">
              {t('cart.total_amount_payable')}
            </span>
            <span className="text-2xl font-bold text-gray-700">
              <FormattedNumber value={4520000} />
            </span>
            <span className="text-sm text-secondary-light">{t('toman')}</span>
          </h3>
        </footer>
      </section>
      <SubmitOrderForm />
    </div>
  );
};

export default ShoppingCart;
