import Radio from '@/components/global/Radio';
import paths from '@/utils/paths';
import { Button, Form } from 'antd';
import { Trans, useTranslation } from 'next-i18next';
import { ChangeEvent, useState } from 'react';

const SubmitOrderForm = () => {
  const { t } = useTranslation('common', { keyPrefix: 'cart' });
  const [checked, setChecked] = useState(false);
  const onCheckboxChange = async (e: ChangeEvent<HTMLInputElement>) => {
    setChecked(e.target.checked);
  };
  const submitHandler = (value: any) => {
    console.log('value: ', value);
  };

  return (
    <section className="my-8">
      <h3 className="text-lg text-secondary-main">{t('gateway_select')}</h3>
      <Form className="w-full" onFinish={submitHandler}>
        <Form.Item name="gateway" className="mb-6">
          <div className="flex gap-3 mt-6">
            <Radio
              className="gateway"
              name="gateway"
              imgSrc="/media/icons/pasargad.svg"
              id="b0"
              label={t('pasargad_gateway')}
              labelClassName="w-24 h-24 md:w-28 md:h-28 text-xs border-2 border-gray-main"
              defaultChecked
            />
            <Radio
              className="gateway"
              name="gateway"
              imgSrc="/media/icons/saman.svg"
              id="b1"
              label={t('saman_gateway')}
              labelClassName="w-24 h-24 md:w-28 md:h-28 text-xs border-2 border-gray-main"
            />
          </div>
        </Form.Item>
        <Form.Item
          name="role"
          className="my-6"
          rules={[
            {
              validator: async () => {
                console.log('checkbox: ', checked);
                if (!checked) {
                  return Promise.reject(new Error(t('conditions_error')));
                }
              },
            },
          ]}
        >
          <label
            htmlFor="conditions"
            className="flex items-center text-base text-secondary-main"
          >
            <input
              id="conditions"
              type="checkbox"
              checked={checked}
              onChange={onCheckboxChange}
              className="border-2 rounded-full outline-none cursor-pointer mie-2 w-7 h-7 border-blue-light checked:border-0 checked:bg-blue-main focus:outline-none focus:ring-0 ring-0"
            />
            <Trans
              components={[
                <span key={1} />,
                <a
                  key={2}
                  href={paths.rules}
                  target="_blank"
                  rel="noreferrer"
                  className="mx-1 font-bold text-black"
                />,
                <span key={3} />,
              ]}
            >
              {t('conditions')}
            </Trans>
          </label>
        </Form.Item>
        <div className="flex justify-end w-full my-8">
          <Button
            className="w-full h-12 text-white rounded-md bg-blue-main md:w-60"
            htmlType="submit"
          >
            {t('order_submit')}
          </Button>
        </div>
      </Form>
    </section>
  );
};

export default SubmitOrderForm;
