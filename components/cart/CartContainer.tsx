import ShoppingCart from '@/components/cart/ShoppingCart';
import useBoundStore from '@/store/store';
import { ORDER_HISTORY } from '@/utils/constants';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';

const CartContainer = () => {
  const { t } = useTranslation('common', { keyPrefix: 'cart' });
  const token = useBoundStore((state) => state.token);

  return (
    <>
      <div className="bg-gray-main">
        <div className="container flex items-center w-full p-2 md:gap-2">
          <button className="w-1/2 p-2 bg-white border rounded-md text-blue-main md:w-32">
            {t('recent_order')}
          </button>
          <Link href={ORDER_HISTORY + `?token=${token}`}>
            <button className="w-1/2 p-2 bg-gray-100 border rounded-md md:w-32 text-secondary-main hover:bg-white">
              {t('order_history')}
            </button>
          </Link>
        </div>
      </div>
      <section className="container ">
        <ShoppingCart />
      </section>
    </>
  );
};

export default CartContainer;
