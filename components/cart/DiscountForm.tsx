import { Button, Form, Input } from 'antd';
import { useTranslation } from 'react-i18next';

const DiscountForm = () => {
  const { t } = useTranslation('common', { keyPrefix: 'cart' });

  return (
    <Form className="flex items-center w-full gap-4">
      <Form.Item className="flex-grow !mb-0 md:flex-grow-0">
        <Input
          placeholder={t('discount_code')}
          className="w-full h-12 rounded-md md:w-48"
        />
      </Form.Item>
      <Button
        htmlType="submit"
        className="w-32 h-12 text-xs font-bold text-white rounded-md bg-blue-main"
      >
        {t('submit_discount')}
      </Button>
    </Form>
  );
};

export default DiscountForm;
