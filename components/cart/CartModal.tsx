import clsx from 'clsx';
import { Portal } from '@/components/global/Portal';
import { CloseOutlined } from '@ant-design/icons';
import useBoundStore from '@/store/store';
import { shallow } from 'zustand/shallow';
import { useTranslation } from 'next-i18next';
import Product from '@/components/cart/Product';
import { FormattedNumber } from 'react-intl';
import { useRouter } from 'next/router';

const products = [
  { title: 'بسته تحلیلی 1', price: 450000 },
  { title: 'بسته تحلیلی 2', price: 26500 },
  { title: 'بسته تحلیلی 1', price: 450000 },
  { title: 'بسته تحلیلی 2', price: 26500 },
  { title: 'بسته تحلیلی 1', price: 450000 },
  { title: 'بسته تحلیلی 2', price: 26500 },
  { title: 'بسته تحلیلی 1', price: 450000 },
  { title: 'بسته تحلیلی 2', price: 26500 },
];

const CartModal = () => {
  const { t } = useTranslation('common');
  const { push } = useRouter();
  const { showCart, setShowCart } = useBoundStore(
    (state) => ({ showCart: state.showCart, setShowCart: state.setShowCart }),
    shallow,
  );
  const hideCartHandler = () => {
    setShowCart(false);
  };
  const checkoutHandler = () => {
    push('/cart').then(() => {
      setShowCart(false);
    });
  };

  return (
    <Portal>
      <div
        className={clsx(
          'fixed z-50 w-screen h-screen block-start-0 inline-start-0 bg-[#61616118] backdrop-blur-[2px]',
          showCart ? 'visible' : 'invisible',
        )}
        onClick={hideCartHandler}
      />
      <div
        className={clsx(
          'w-full sm:w-[500px] p-4 h-screen z-[60] flex flex-col fixed bg-white top-0 gap-6 transition-all flx-col',
          showCart
            ? 'inline-start-0 visible'
            : '-inline-start-full sm:-inline-start-[500px] invisible',
        )}
      >
        <header className="flex items-center justify-between py-1 border-b">
          <h2 className="text-lg font-bold text-secondary-main">
            {t('cart.products')}
          </h2>
          <button
            onClick={hideCartHandler}
            className="w-12 h-12 p-2 transition-all rounded-md bg-gray-light hover:bg-gray-main"
          >
            <CloseOutlined className="text-lg font-bold text-secondary-main" />
          </button>
        </header>
        <main className="overflow-y-auto divide-y custom-scroll">
          {products.map((item, i) => (
            <Product key={i} type="modal" {...item} />
          ))}
        </main>
        <footer className="flex flex-col gap-2">
          <div className="flex items-center justify-center w-full gap-3 bg-gray-300 rounded-md h-14">
            <span className="text-gray-700">{t('cart.total')}</span>
            <span className="text-lg font-bold text-gray-700">
              <FormattedNumber value={4520000} />
            </span>
            <span className="text-sm text-secondary-light">{t('toman')}</span>
          </div>
          <button
            onClick={checkoutHandler}
            className="w-full text-lg font-bold text-white transition-all bg-red-500 rounded-md h-14 hover:bg-red-600"
          >
            {t('cart.checkout')}
          </button>
        </footer>
      </div>
    </Portal>
  );
};

export default CartModal;
