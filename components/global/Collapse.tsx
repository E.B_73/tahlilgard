import clsx from 'clsx';
import { ReactNode } from 'react';
import { DivProps } from 'react-html-props';

interface CollapseProps extends DivProps {
  show?: boolean;
  children?: ReactNode;
  className?: string;
}
const Collapse = ({ show, children, className, ...rest }: CollapseProps) => {
  return (
    <div
      className={clsx(
        '!overflow-hidden !max-h-0',
        className,
        show
          ? '!max-h-screen transition-all duration-500'
          : '!max-h-0 invisible',
      )}
      {...rest}
    >
      {children}
    </div>
  );
};

export default Collapse;
