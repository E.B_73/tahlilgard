import { useRef, useEffect, useState, ReactNode } from 'react';
import { createPortal } from 'react-dom';
import { DivProps } from 'react-html-props';

interface PortalProps extends DivProps {
  children: ReactNode;
  className?: string;
}

export const Portal = ({ children, className, ...rest }: PortalProps) => {
  const ref = useRef<Element | null>(null);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    ref.current = document.querySelector<HTMLElement>('#portal');
    setMounted(true);
  }, []);

  return mounted && ref.current
    ? createPortal(
        <div className={className} {...rest}>
          {children}
        </div>,
        ref.current,
      )
    : null;
};
