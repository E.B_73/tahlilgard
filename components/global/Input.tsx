import React, { Dispatch, FC, SetStateAction } from 'react';
import classes from '@/styles/sass/signup.module.scss';
import { useTranslation } from 'next-i18next';

interface inputProps {
  type: 'password' | 'input' | 'number';
  label: string;
  required?: boolean;
  changeTarget?: Dispatch<SetStateAction<string | undefined>>;
  ltr?: boolean;
  style?: React.CSSProperties;
}
const Input: FC<inputProps> = ({
  type,
  label,
  required,
  changeTarget,
  ltr,
  style = {},
}) => {
  const { t } = useTranslation();
  return (
    <div
      className={classes.field + ' ' + classes.form__group}
      style={{ ...style }}
    >
      <input
        onChange={(e) => {
          if (changeTarget) {
            console.log(e.target.value);
            changeTarget(e.target.value);
          }
        }}
        type={type}
        className={classes.form__field}
        placeholder={label}
        name={label}
        id={label}
        required={required}
        style={{ direction: ltr ? 'ltr' : 'inherit' }}
      />
      <label htmlFor={label} className={classes.form__label}>
        {t(label as any)}
      </label>
    </div>
  );
};
export default Input;
