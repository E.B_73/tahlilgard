import { Modal } from 'antd';
import { Dispatch, ReactNode, SetStateAction } from 'react';

interface CustomModalProps {
  title?: string;
  children?: ReactNode;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  okText?: string;
  cancelText?: string;
  footer?: ReactNode;
  className?: string;
}

const CustomModal = ({
  okText,
  cancelText,
  children,
  title,
  open = false,
  setOpen,
  footer,
  className,
}: CustomModalProps) => {
  return (
    <Modal
      title={title}
      centered
      open={open}
      onOk={() => setOpen(false)}
      onCancel={() => setOpen(false)}
      cancelText={cancelText}
      okText={okText}
      footer={footer}
      className={className}
      bodyStyle={{
        overflowY: 'auto',
        maxHeight: 'calc(100vh - 200px)',
        padding: 0,
      }}
    >
      {children}
    </Modal>
  );
};

export default CustomModal;
