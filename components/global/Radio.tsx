import clsx from 'clsx';
import { InputProps } from 'react-html-props';
import Image from 'next/image';
interface RadioProps extends InputProps {
  label?: string;
  id: string;
  imgSrc?: string;
  className?: string;
  labelClassName?: string;
}
const Radio = ({
  label,
  id,
  imgSrc,
  className,
  labelClassName,
  ...rest
}: RadioProps) => {
  return (
    <div>
      <input
        type="radio"
        className={clsx('hidden', className)}
        id={id}
        {...rest}
      ></input>
      <label
        role="button"
        className={clsx(
          'py-2 transition-all',
          labelClassName,
          imgSrc &&
            'flex flex-col items-center gap-2 p-4 rounded-lg w-20 h-20 md:w-24 md:h-24',
        )}
        htmlFor={id}
      >
        {imgSrc && (
          <div className="relative w-6 h-6 md:w-9 md:h-9">
            <Image
              className="relative max-w-full max-h-full"
              layout="fill"
              objectFit="contain"
              src={imgSrc}
              alt="category-picture"
            />
          </div>
        )}
        <span className="text-center">{label}</span>
      </label>
    </div>
  );
};

export default Radio;
