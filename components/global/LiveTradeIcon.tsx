import { useTranslation } from 'next-i18next';
import { CiStreamOn } from 'react-icons/ci';

type LiveTradeIconProps = {
  icon?: string;
};

const LiveTradeIcon = ({ icon }: LiveTradeIconProps) => {
  const { t } = useTranslation('liveTrade');

  switch (icon) {
    case 'finished':
      return (
        <span className="p-1 text-xs text-white bg-red-600 rounded-md">
          {t('finished')}
        </span>
      );
    case 'live':
      return (
        <CiStreamOn className="w-8 font-bold text-white bg-green-600 rounded-md" />
      );
    case 'future':
      return (
        <span className="p-1 text-xs text-white bg-blue-600 rounded-sm">
          {t('future')}
        </span>
      );
    default:
      return <></>;
  }
};
export default LiveTradeIcon;
