import { Dispatch, FC, SetStateAction } from 'react';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import { Typography } from 'antd';
const Timer: FC<{
  duration: number;
  completeHandler: Dispatch<SetStateAction<boolean>>;
}> = ({ duration, completeHandler }) => {
  return (
    <CountdownCircleTimer
      duration={duration}
      colors={['#0096d6', '#0096d6', '#FF0000']}
      colorsTime={[120, 11, 9]}
      onComplete={() => {
        completeHandler(false);
      }}
      isPlaying={true}
      size={32}
      strokeWidth={2}
    >
      {({ remainingTime, color }) => (
        <Typography.Text style={{ color, fontSize: '0.75em' }}>
          {remainingTime}
        </Typography.Text>
      )}
    </CountdownCircleTimer>
  );
};
export default Timer;
