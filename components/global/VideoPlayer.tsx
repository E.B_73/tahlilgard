import dynamic from 'next/dynamic';
const ReactPlayer = dynamic(() => import('react-player'), { ssr: false });

interface VideoPlayerProps {
  videoUrl: string;
  thumbnailUrl: string;
}

const VideoPlayer = ({ videoUrl, thumbnailUrl }: VideoPlayerProps) => {
  return (
    <ReactPlayer
      className="!top-0 !left-0 !absolute"
      width="100%"
      height="100%"
      controls
      playing
      config={{
        file: {
          attributes: {
            controlsList: 'nodownload', // <- this is the important bit
          },
        },
      }}
      playIcon={
        // eslint-disable-next-line @next/next/no-img-element
        <img alt="Play" src="/media/icons/play-icon.png" className="w-10" />
      }
      url={videoUrl}
      light={thumbnailUrl}
    />
  );
};

export default VideoPlayer;
