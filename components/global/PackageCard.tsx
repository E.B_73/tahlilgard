import { Button, Tooltip } from 'antd';
import Image from 'next/image';
import {
  ShoppingCartOutlined,
  HeartOutlined,
  LeftOutlined,
} from '@ant-design/icons';
import { FormattedNumber } from 'react-intl';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import clsx from 'clsx';
import LiveTradeIcon from '@/components/global/LiveTradeIcon';

export interface PackageCardProps {
  type: 'learning' | 'live-trade';
  title: string;
  liveTradeType?: 'live' | 'finished' | 'future';
  imgUrl: string;
  link: string;
  price?: number;
  className?: string;
}
const PackageCard = ({
  type,
  title,
  liveTradeType,
  imgUrl,
  link,
  price = 0,
  className,
}: PackageCardProps) => {
  const { t } = useTranslation('learning');

  return (
    <Link href={link}>
      <div
        className={clsx(
          'w-full space-y-2 overflow-hidden bg-white rounded-md shadow-md sm:w-80 mx-auto sm:mx-0 cursor-pointer',
          className,
        )}
      >
        <div className="relative w-full h-36">
          {type === 'learning' && (
            <Tooltip title={t('add_favorite')}>
              <HeartOutlined className="absolute left-0 z-20 m-2 text-xl cursor-pointer" />
            </Tooltip>
          )}
          {type === 'live-trade' && liveTradeType && (
            <div className="absolute left-0 z-20 m-2 text-xl">
              <LiveTradeIcon icon={liveTradeType} />
            </div>
          )}
          <Image
            className="relative max-w-full max-h-full"
            layout="fill"
            src={imgUrl}
            objectFit="cover"
          />
        </div>
        <h2 className="w-full p-2 pb-0 mb-3 font-light min-h-28 line-clamp-2">
          {title}
        </h2>
        <div className="flex items-center justify-between h-10 gap-2 bg-neutral-100">
          {price ? (
            <h2 className="flex gap-1 font-bold mis-1">
              <FormattedNumber value={price} />
              <span>{t('single_course.toman')}</span>
            </h2>
          ) : (
            price === 0 && t('free')
          )}

          <Link href={link}>
            <Button
              icon={
                price ? (
                  <ShoppingCartOutlined />
                ) : (
                  price === 0 && <LeftOutlined className="!text-sm icon" />
                )
              }
              className="!bg-blue-300 !h-full !w-12 !text-white rounded-be-md rounded-is-none"
            />
          </Link>
        </div>
      </div>
    </Link>
  );
};

export default PackageCard;
