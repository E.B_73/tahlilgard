import clsx from 'clsx';
import { SVGProps } from 'react-html-props';
const FacebookSVG = ({ className, ...props }: SVGProps) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="9.455"
      height="16"
      viewBox="0 0 9.455 16"
      className={clsx('w-full h-full fill-current', className)}
      {...props}
    >
      <path
        d="M15.545,1.5h2.182a.727.727,0,0,1,.727.727V5.136a.727.727,0,0,1-.727.727H15.545V7.318h2.182a.727.727,0,0,1,.706.9l-.727,2.909a.727.727,0,0,1-.706.551H15.545v5.091a.727.727,0,0,1-.727.727H11.909a.727.727,0,0,1-.727-.727V11.682H9.727A.727.727,0,0,1,9,10.955V8.045a.727.727,0,0,1,.727-.727h1.455V5.864A4.369,4.369,0,0,1,15.545,1.5ZM17,2.955H15.545a2.912,2.912,0,0,0-2.909,2.909V8.045a.727.727,0,0,1-.727.727H10.455v1.455h1.455a.727.727,0,0,1,.727.727v5.091h1.455V10.955a.727.727,0,0,1,.727-.727h1.614L16.8,8.773H14.818a.727.727,0,0,1-.727-.727V5.864a1.456,1.456,0,0,1,1.455-1.455H17Z"
        transform="translate(-9 -1.5)"
        fill="#fff"
      />
    </svg>
  );
};

export default FacebookSVG;
