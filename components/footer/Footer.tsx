import {
  FaDownload,
  FaTelegram,
  FaWhatsapp,
  FaInstagram,
} from 'react-icons/fa';
import { Button } from 'antd';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import { email, tell } from '@/utils/constants';
import WhatsAppSVG from '@/components/svg/WhatsAppSVG';
import paths from '@/utils/paths';

const Footer = () => {
  const { t } = useTranslation('common', { keyPrefix: 'footer' });

  return (
    <footer className="bg-primary-dark">
      <div className="container py-3 space-y-12">
        <div className="flex flex-col flex-wrap w-full gap-y-6 gap-x-4 md:flex-row md:justify-between md:items-center">
          <div className="flex items-center gap-4">
            <div className="relative w-14 h-14">
              <Image
                src="/media/images/delivery-truck.png"
                alt="icon"
                layout="fill"
              />
            </div>
            <div className="flex flex-col gap-2">
              <h2 className="text-lg text-white">{t('features.title_1')}</h2>
              <h3 className="text-base text-gray-main">
                {t('features.sub_title_1')}
              </h3>
            </div>
          </div>
          <div className="flex items-center gap-4">
            <div className="relative w-14 h-14">
              <Image
                src="/media/images/credit-card.png"
                alt="icon"
                layout="fill"
              />
            </div>
            <div className="flex flex-col gap-2">
              <h2 className="text-lg text-white">{t('features.title_2')}</h2>
              <h3 className="text-base text-gray-main">
                {t('features.sub_title_2')}
              </h3>
            </div>
          </div>
          <div className="flex items-center gap-4">
            <div className="relative w-14 h-14">
              <Image src="/media/images/clerk.png" alt="icon" layout="fill" />
            </div>
            <div className="flex flex-col gap-2">
              <h2 className="text-lg text-white">{t('features.title_3')}</h2>
              <h3 className="text-base text-gray-main">
                {t('features.sub_title_3')}
              </h3>
            </div>
          </div>
          <div className="flex items-center gap-4">
            <div>
              <div className="relative w-14 h-14">
                <Image
                  src="/media/images/police-officer.png"
                  alt="icon"
                  layout="fill"
                />
              </div>
            </div>
            <div className="flex flex-col gap-2">
              <h2 className="text-lg text-white">{t('features.title_4')}</h2>
              <h3 className="text-base text-gray-main">
                {t('features.sub_title_4')}
              </h3>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-start justify-between order-last w-full md:flex-row md:order-first md:items-start gap-x-6 gap-y-8">
          <div className="flex flex-col gap-4 transition-all">
            <a className="text-gray-400 hover:text-white">
              {t('links.about_us')}
            </a>
            <a className="text-gray-400 hover:text-white">{t('links.rules')}</a>
            <a className="text-gray-400 hover:text-white">
              {t('links.questions')}
            </a>
          </div>
          <div className="flex flex-col gap-4 transition-all">
            <a className="text-gray-400 hover:text-white">
              {t('links.returned_rules')}
            </a>
            <a className="text-gray-400 hover:text-white">
              {t('links.provider_guide')}
            </a>
            <a className="text-gray-400 hover:text-white">
              {t('links.purchase_guide')}
            </a>
          </div>
          <div className="flex flex-col gap-4 transition-all">
            <a className="text-gray-400 hover:text-white">
              {t('links.permissions')}
            </a>
            <a className="text-gray-400 hover:text-white">
              {t('links.contact_us')}
            </a>
            <a className="text-gray-400 hover:text-white">
              {t('links.newsletters')}
            </a>
          </div>
          <div className="flex flex-col gap-4 transition-all">
            <h2 className="text-gray-400">{t('links.info_1')}</h2>
            <h2 className="text-gray-400">{t('links.info_2')}</h2>
            <div className="flex items-center gap-2">
              <h2 className="text-gray-400">{t('links.tell')}</h2>
              <a
                className="text-sm text-gray-400 cursor-pointer hover:text-white"
                href={`tell:${tell}`}
                target="_blank"
                rel="noreferrer"
              >
                {t('links.tell_number')}
              </a>
            </div>
            <div className="flex items-center gap-2">
              <h2 className="text-gray-400">{t('links.email')}</h2>
              <a
                className="text-sm text-gray-400 cursor-pointer hover:text-white"
                href={`mailto:${email}`}
                target="_blank"
                rel="noreferrer"
              >
                {email}
              </a>
            </div>
          </div>
          <div className="flex flex-col items-center order-first w-full mx-auto md:mx-0 md:w-auto md:items-start gap-y-8 md:order-last">
            <Button
              icon={
                <FaDownload className="transition-all text-blue-main group-hover:text-white" />
              }
              className="flex items-center justify-center w-full gap-2 p-5 text-sm transition-all bg-transparent border rounded-full hover:text-white text-blue-main group hover:bg-blue-main border-blue-main"
            >
              {t('links.download_app')}
            </Button>
            <div className="flex items-center justify-center w-full gap-2">
              <div className="p-3 transition-all border rounded-full cursor-pointer border-blue-main hover:bg-blue-light">
                <FaInstagram className="text-white" size={25} />
              </div>
              <div className="p-3 transition-all border rounded-full cursor-pointer border-blue-main hover:bg-blue-light">
                <FaWhatsapp className="text-white" size={25} />
              </div>
              <div className="p-3 transition-all border rounded-full cursor-pointer border-blue-main hover:bg-blue-light">
                <FaTelegram className="text-white" size={25} />
              </div>
            </div>
            <div className="flex items-center justify-center w-full gap-4 md:justify-between">
              <Link href={paths.home}>
                <a className="relative block w-24 h-24 overflow-hidden rounded-lg">
                  <Image
                    layout="fill"
                    objectFit="fill"
                    src="/media/images/enamad.jpg"
                  />
                </a>
              </Link>
              <Link href={paths.home}>
                <a className="relative block w-24 h-24 overflow-hidden rounded-lg">
                  <Image
                    layout="fill"
                    objectFit="fill"
                    src="/media/images/enamad.jpg"
                  />
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="flex justify-center w-full pt-3 border-t">
          <h4 className="text-sm text-white">{t('right')}</h4>
        </div>
      </div>

      {/* <button
        onClick={() => {
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }}
        className="fixed z-40 flex items-center justify-center w-10 h-10 bg-white rounded-full inline-end-3 bottom-7 ms-auto"
      >
        <IoIosArrowUp />
      </button> */}

      <div className="fixed flex items-center justify-between w-full bottom-7 inline-start-3">
        <button className="flex items-center gap-2">
          <WhatsAppSVG className="w-10 h-10" />
          <h5 className="p-2 m-0 text-gray-700 bg-white rounded-md ">
            {t('help')}
          </h5>
        </button>
      </div>
    </footer>
  );
};

export default Footer;
