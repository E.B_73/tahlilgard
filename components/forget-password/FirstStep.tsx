import { useState } from 'react';
import { LoadingOutlined } from '@ant-design/icons';
import ErrorHandler from '@/utils/toast-handler/ErrorHandler';
import { Button, Form, Input } from 'antd';
import Link from 'next/link';
import Timer from '@/components/global/Timer';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import useSendSecondForgetPassMutation from '@/hooks/mutations/useSendSecondForgetPassMutation';
import useSendResendCodeMutation from '@/hooks/mutations/useSendResendCodeMutation';
import paths from '@/utils/paths';

const validateCode = (code: string) => {
  if (/^\d{1,5}$/g.test(code)) return code;
  else if (/^\d{6}$/g.test(code)) return code.slice(0, 5);
};

const FirstStep = () => {
  const { t } = useTranslation('common');
  const [showTimer, setShowTimer] = useState<boolean>(true);
  const { countDown, recordId } = useAppStore(
    (state) => ({
      countDown: state.forgetPassCountDown,
      recordId: state.forgetPassRecordId,
    }),
    shallow,
  );
  const { sendPass, loading } = useSendSecondForgetPassMutation();
  const { sendResendCode, loading: reSendLoading } =
    useSendResendCodeMutation('forget_pass_form');

  const reSendHandler = async () => {
    await sendResendCode({
      variables: {
        reSendVerifyCodeInput: {
          recordId,
        },
      },
    });
    setShowTimer(true);
  };

  const submitHandler = async (values: { code: string }) => {
    try {
      await sendPass({
        variables: {
          forgetPasswordInput: {
            recordId,
            code: +values.code,
          },
        },
      });
    } catch (e) {
      ErrorHandler(t('An Error occured, Try Again'));
    }
  };

  return (
    <div className="flex flex-col gap-4">
      <Form
        wrapperCol={{ span: 24 }}
        layout="vertical"
        onFinish={submitHandler}
        initialValues={{ isProvider: false }}
      >
        <Input.Group
          compact
          className="!flex !items-center !justify-center"
          style={{ marginBottom: '24px' }}
        >
          <Form.Item
            name="code"
            className="!m-0 !w-3/5 md:!w-30"
            normalize={validateCode}
          >
            <Input
              className="!h-10 !rounded-ie-lg ltr placeholder:!text-3xl !text-xl placeholder:!text-center placeholder:!-translate-y-2 !text-center !font-bold"
              type="number"
              maxLength={5}
              placeholder="_ _ _ _ _ "
            />
          </Form.Item>
          {showTimer && countDown ? (
            <div className="bg-white !w-2/5 md:!w-20 !h-10  !rounded-ie-lg border-[1px] !flex !justify-center !items-center">
              <Timer duration={countDown} completeHandler={setShowTimer} />
            </div>
          ) : (
            <Button
              disabled={reSendLoading}
              type="default"
              size="middle"
              onClick={reSendHandler}
              className="!w-2/5 md:!w-25 !h-10 !rounded-ie-lg"
            >
              {reSendLoading ? <LoadingOutlined /> : t('Resend Code')}
            </Button>
          )}
        </Input.Group>
        <Form.Item wrapperCol={{ md: { span: 24 } }}>
          <Button
            disabled={loading}
            type="primary"
            htmlType="submit"
            className="!h-10 !rounded-lg !w-full"
          >
            {loading ? <LoadingOutlined /> : t('Send Code')}
          </Button>
        </Form.Item>
      </Form>
      <div>
        {t('Already signed up')}
        <Link href={paths.check_auth}>{t('Login to Tahlilgard')}</Link>
      </div>
    </div>
  );
};

export default FirstStep;
