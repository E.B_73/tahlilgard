import { Row, Col, Typography } from 'antd';
import FirstStep from '@/components/forget-password/FirstStep';
import SecondStep from '@/components/forget-password/SecondStep';
import { useTranslation } from 'next-i18next';
import useDetectUser from '@/hooks/global/useDetectUser';
import useAppStore from '@/store/store';
import Collapse from '@/components/global/Collapse';
import { useEffect } from 'react';
import router from 'next/router';
import { ForgetPassStpEnum } from '@/utils/enums';

const { Title, Text } = Typography;
const Container = () => {
  const { loading } = useDetectUser(true);
  const step = useAppStore((state) => state.forgetPassStep);
  const { t } = useTranslation('common');
  useEffect(() => {
    if (step === ForgetPassStpEnum.START) router.replace('/check-auth');
  }, []);
  return (
    <>
      <Row
        justify="center"
        align="middle"
        className="rounded-3xl !bg-primary-main"
      >
        {loading ? (
          <div>loading</div>
        ) : (
          step !== 'neutral' && (
            <>
              <Col md={12} sm={24} className="!p-8 w-full flex flex-col gap-4">
                <h2 className="mb-6 text-xl font-semibold">
                  {t('Forget Password')}
                </h2>
                <Collapse show={step === ForgetPassStpEnum.SEND_CODE}>
                  <FirstStep />
                </Collapse>
                <Collapse show={step === ForgetPassStpEnum.NEW_PASS}>
                  <SecondStep />
                </Collapse>
              </Col>
              <Col
                md={12}
                sm={24}
                className="!p-8 md:border-is-2 border-bs-2 md:border-bs-0"
              >
                <Title level={5}>
                  {t('You must have account to use services')}
                </Title>
                <Text>{t('Signup is free')}</Text>
                <Text>{t('You can use of Analysis after signup')}</Text>
              </Col>
            </>
          )
        )}
      </Row>
    </>
  );
};

export default Container;
