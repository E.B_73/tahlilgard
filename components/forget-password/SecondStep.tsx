// styles
import { LoadingOutlined } from '@ant-design/icons';
// components
import ErrorHandler from '@/utils/toast-handler/ErrorHandler';
import { Button, Form, Input } from 'antd';
// hooks
import { useTranslation } from 'next-i18next';
import useSendThirdForgetPassMutation from '@/hooks/mutations/useSendThirdForgetPassMutation';
// zustsnd
import useAppStore from '@/store/store';

const SecondStep = () => {
  // translate hook
  const { t } = useTranslation('common');
  // mutation hook
  const { sendPass, loading } = useSendThirdForgetPassMutation();
  // use store
  const recordId = useAppStore((state) => state.forgetPassRecordId);

  // submitHandler for login
  const submitHandler = async (values: {
    password: string;
    confirmPassword: string;
  }) => {
    try {
      if (values.password)
        await sendPass({
          variables: {
            forgetPasswordStepThreeInput: {
              recordId,
              newPassword: values.password,
            },
          },
        });
    } catch (e) {
      ErrorHandler(t('An Error occured, Try Again'));
    }
  };

  return (
    <Form wrapperCol={{ span: 24 }} layout="vertical" onFinish={submitHandler}>
      <Form.Item
        style={{ marginBottom: '24px' }}
        name="password"
        rules={[
          {
            required: true,
            message: t('Please input your password!'),
          },
        ]}
      >
        <Input.Password
          className="!h-10 !rounded-lg"
          placeholder={t('New password')}
        />
      </Form.Item>
      <Form.Item
        style={{ marginBottom: '24px' }}
        name="confirmPassword"
        dependencies={['password']}
        rules={[
          {
            required: true,
            message: t('Please confirm your password!'),
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error(t('Passwords doesnt Equal')));
            },
          }),
        ]}
      >
        <Input.Password
          className="!h-10 !rounded-lg"
          placeholder={t('Confirm Password')}
        />
      </Form.Item>
      <Form.Item wrapperCol={{ md: { span: 24 } }}>
        <Button
          disabled={loading}
          type="primary"
          htmlType="submit"
          className="!h-10 !rounded-lg !w-full"
        >
          {loading ? <LoadingOutlined /> : t('Change and login')}
        </Button>
      </Form.Item>
    </Form>
  );
};

export default SecondStep;
