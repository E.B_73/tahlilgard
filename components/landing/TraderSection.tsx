import paths from '@/utils/paths';
import { Button, Typography } from 'antd';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
const { Title, Text } = Typography;

const TraderSection = () => {
  const { t } = useTranslation('common');

  return (
    <div className="flex flex-col justify-center bg-[url('/media/images/traders_bg.jpg')] bg-cover bg-fixed bg-no-repeat bg-top h-screen">
      <div className="container">
        <div className="p-6 rounded-[35px] bg-gray-main md:w-96">
          <Title level={2} className="mb-0">
            {t('Tahlilgard')}
          </Title>
          <Title level={3} className="mt-0">
            {t('Analysis Of Stock')}
          </Title>
          <Text style={{ color: '#003248' }}>
            {t('You Need the Assist Of Professional Traders')}
          </Text>
          <div className="flex gap-2 mt-1">
            <Link href={paths.check_auth}>
              <Button type="primary" className="rounded-[35px]">
                {t('Analysis Forum')}
              </Button>
            </Link>
            <Link href={paths.rules}>
              <Button type="ghost" className="rounded-[35px]">
                {t('Terms and Conditions')}
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};
export default TraderSection;
