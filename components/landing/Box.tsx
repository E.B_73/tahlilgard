import Image from 'next/image';
import { Row, Col, Typography, Button } from 'antd';
const { Title, Text } = Typography;
import clsx from 'clsx';
interface BoxProps {
  className?: string;
  reverse?: boolean;
  children: string;
  title: string;
  img: string;
  btnText: string;
  btnLink: string;
}

const Box = ({
  className,
  children,
  title,
  img,
  btnLink,
  btnText,
  reverse,
}: BoxProps) => {
  return (
    <div
      className={clsx(
        reverse ? 'bg-primary-main text-black' : 'bg-primary-dark text-white',
        className,
      )}
    >
      <Row justify="space-around" align="middle" className="container py-4">
        <Col
          md={{ span: 12, order: reverse ? 2 : 1 }}
          sm={{ span: 24, order: reverse ? 2 : 1 }}
          style={{ padding: '1em' }}
        >
          <Title className={clsx(reverse ? 'text-black' : 'text-white')}>
            {title}
          </Title>
          <Text className={clsx(reverse ? 'text-black' : 'text-white')}>
            {children}
          </Text>
          <div className="mt-2">
            <Button type="primary" href={btnLink} className="rounded-md">
              {btnText}
            </Button>
          </div>
        </Col>
        <Col
          className="relative overflow-hidden h-96 rounded-3xl"
          md={{ span: 12, order: reverse ? 1 : 2 }}
          sm={{ span: 24, order: reverse ? 1 : 2 }}
        >
          <Image
            layout="fill"
            objectFit="fill"
            src={img}
            alt="img"
            className="relative w-full h-full"
          />
        </Col>
      </Row>
    </div>
  );
};

export default Box;
