import Section from '@/components/landing/Box';
import { useTranslation } from 'next-i18next';

const LoginSection = () => {
  const { t } = useTranslation('common');

  return (
    <>
      <Section
        title={t('How To Use Tahlilgard')}
        img="/media/images/sample-baner.png"
        btnLink="/check-auth"
        btnText={t('login_to_panel')}
      >
        {t(
          'You should watch how to work with tahlilgard. if You Have any question, Visit FAQ Page Or Contact Support@tahlilgard.com',
        )}
      </Section>
      <Section
        title={t('How to Signup')}
        img="/media/images/sample-baner.png"
        btnLink="/check-auth"
        btnText={t('Signup')}
        reverse
      >
        {t(
          'You should watch how to work signup in tahlilgard. if You Have any question, Visit FAQ Page Or Contact Support@tahlilgard.com',
        )}
      </Section>
    </>
  );
};

export default LoginSection;
