import { Typography, Button } from 'antd';
const { Title, Text } = Typography;
import Image from 'next/image';
import { Fade } from 'react-reveal';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';
import clsx from 'clsx';

interface BoxProps {
  img: string;
  title: string;
  children: string;
  right?: boolean;
  link?: string;
}
const RoadMapBox = ({ right, img, title, children, link }: BoxProps) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  return (
    <Fade left={!right} right={right} distance="30px" duration={1500}>
      <div
        className={clsx(
          'container flex justify-center items-center mb-6',
          right ? 'flex-row-reverse' : 'flex-row',
        )}
      >
        <div
          className={clsx(
            'w-full md:max-w-[200px] text-justify',
            right ? 'md:mis-4' : 'md:mie-4',
          )}
        >
          <Title level={4}>{title}</Title>
          <Text>{children}</Text>
          <div className="mt-4">
            {link && (
              <Button
                className="rounded-md"
                type="ghost"
                onClick={() => {
                  router.push(link);
                }}
              >
                {t('More')}
              </Button>
            )}
          </div>
        </div>
        <span className="hidden w-2/6 h-[3px] bg-blue-light md:block" />
        <div className="relative w-32 h-32 hidden border-[3px] rounded-[35px] md:block border-blue-light p-4">
          <Image
            width={100}
            height={100}
            layout="responsive"
            src={img}
            alt="logo"
          />
        </div>
      </div>
    </Fade>
  );
};

export default RoadMapBox;
