import { useTranslation } from 'next-i18next';
import BigCircle from '@/components/landing/BigCircle';
import RoadMap from '@/components/landing/RoadMapBox';
import paths from '@/utils/paths';
const WhyUsSection = () => {
  const { t } = useTranslation('common');
  return (
    <div className="w-full py-4 bg-gray-main">
      <BigCircle />
      <RoadMap
        title={t('Analysis Forum')}
        img="/media/icons/online-icon.svg"
        link={paths.analysis_forum}
      >
        {t('road_map.describe_1')}
      </RoadMap>
      <RoadMap
        right
        title={t('Simple and Comprehensible')}
        img="/media/icons/tahlil-icon.svg"
      >
        {t('road_map.describe_2')}
      </RoadMap>
      <RoadMap
        title={t('Various Markets')}
        img="/media/icons/technical-icon.svg"
      >
        {t('road_map.describe_3')}
      </RoadMap>
      <RoadMap right title={t('Tutorial')} img="/media/icons/tahlil-icon.svg">
        {t('road_map.describe_4')}
      </RoadMap>
      <RoadMap
        title={t('Money back guarantee')}
        img="/media/icons/investment-icon.svg"
      >
        {t('road_map.describe_5')}
      </RoadMap>
      <RoadMap
        title={t('Cooperation')}
        img="/media/icons/notices-icon.svg"
        right
      >
        {t('road_map.describe_6')}
      </RoadMap>
    </div>
  );
};

export default WhyUsSection;
