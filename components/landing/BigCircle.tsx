import { Typography } from 'antd';
const { Title } = Typography;
import { Fade } from 'react-reveal';

// hooks
import { useTranslation } from 'next-i18next';

const BigCircle = () => {
  const { t } = useTranslation('common', { keyPrefix: 'road_map' });
  return (
    <Fade top distance="50px" duration={2000}>
      <div className="flex items-center justify-center p-4 mx-auto rounded-full w-72 h-72 bg-blue-light">
        <div className="text-center">
          <Title level={5}>{t('title')}</Title>
        </div>
      </div>
    </Fade>
  );
};

export default BigCircle;
