import Radio from '@/components/global/Radio';

interface PackageCategoryProps {
  titleList: string[];
}

const PackageCategory = ({ titleList }: PackageCategoryProps) => {
  return (
    <div className="flex w-full gap-4 py-2 border-b border-neutral-400">
      {titleList.map((title, i) => (
        <Radio
          key={i}
          id={i.toString()}
          name="packages"
          className="sort"
          value={title}
          label={title}
          defaultChecked={i === 0}
        />
      ))}
    </div>
  );
};

export default PackageCategory;
