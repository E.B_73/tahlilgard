import Image from 'next/image';
import Link from 'next/link';

interface CategoryIconWithTextProps {
  text: string;
  imgUrl: string;
  query?: string;
}

const CategoryIconWithText = ({
  imgUrl,
  text,
  query,
}: CategoryIconWithTextProps) => {
  return (
    <Link href={{ pathname: '/explore', query: { category: query } }}>
      <a>
        <div className="flex flex-col items-center gap-2 p-4 border-2 border-transparent rounded-md hover:border-neutral-200 hover:border-2 hover:bg-neutral-100">
          <div className="relative w-20 h-20">
            <Image
              className="relative max-h-full min-w-full"
              layout="fill"
              src={imgUrl}
            />
          </div>
          <h2 className="text-base">{text}</h2>
        </div>
      </a>
    </Link>
  );
};

export default CategoryIconWithText;
