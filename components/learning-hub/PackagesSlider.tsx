import { Swiper as SwiperContainer, SwiperSlide } from 'swiper/react';
import Swiper from 'swiper';
import PackageCard from '@/components/global/PackageCard';
import { Button } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';
import { useTranslation } from 'next-i18next';
import { useCallback, useState } from 'react';

interface PackagesSliderProps {
  slides?: {
    imgUrl: string;
    link: string;
    title: string;
    price: number;
  }[];
  title: string;
}

const PackagesSlider = ({ slides, title }: PackagesSliderProps) => {
  const { t } = useTranslation('learning');
  const [swiperRef, setSwiperRef] = useState<Swiper>();
  const prevSlideHandler = useCallback(() => {
    if (!swiperRef) return;
    swiperRef.slidePrev();
  }, [swiperRef]);
  const nextSlideHandler = useCallback(() => {
    if (!swiperRef) return;
    swiperRef.slideNext();
  }, [swiperRef]);
  return (
    <div className="container relative mb-10 space-y-2">
      <h1 className="text-2xl font-bold">{title}</h1>
      <div className="flex items-center justify-between gap-2">
        <RightOutlined
          className="icon navigation-arrow-swiper"
          onClick={prevSlideHandler}
        />
        <SwiperContainer
          slidesPerView="auto"
          spaceBetween={15}
          onSwiper={setSwiperRef}
          className="my-8 package-swiper"
        >
          {slides?.map((slide, i) => (
            <SwiperSlide key={i} className="cursor-pointer hover:shadow-md">
              <PackageCard
                link={slide.link}
                imgUrl={slide.imgUrl}
                title={slide.title}
                price={slide.price}
                type="learning"
              />
            </SwiperSlide>
          ))}
        </SwiperContainer>
        <LeftOutlined
          className="icon navigation-arrow-swiper"
          onClick={nextSlideHandler}
        />
      </div>
      <Button className="!mis-auto !bg-blue-600 !text-white !flex !items-center">
        <span>{t('all_list')}</span>
        <LeftOutlined className="text-xs icon" />
      </Button>
    </div>
  );
};

export default PackagesSlider;
