import CategoryIconWithText from '@/components/learning-hub/CategoryIconWithText';
import { useTranslation } from 'next-i18next';

const SubjectsCategory = () => {
  const { t } = useTranslation('learning', { keyPrefix: 'subjects' });
  return (
    <section className="container my-4 space-y-4">
      <h1 className="text-2xl font-bold text-center">{t('title')}</h1>
      <div className="flex flex-wrap justify-center gap-6 justify-items-center">
        <CategoryIconWithText
          text={t('programming')}
          imgUrl="/media/icons/monitoring.png"
          query="programing"
        />
        <CategoryIconWithText
          text={t('blockchain')}
          imgUrl="/media/icons/stock.png"
          query="programing"
        />
        <CategoryIconWithText
          text={t('bourse')}
          imgUrl="/media/icons/online-learning.png"
          query="programing"
        />
        <CategoryIconWithText
          text={t('webinar')}
          imgUrl="/media/icons/webinar.png"
          query="programing"
        />
      </div>
    </section>
  );
};

export default SubjectsCategory;
