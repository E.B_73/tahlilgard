import { AutoComplete, AutoCompleteProps } from 'antd';
import { useTranslation } from 'next-i18next';

const SearchLearning = (props: AutoCompleteProps) => {
  const { t } = useTranslation('learning');
  return (
    <AutoComplete
      className="w-full auto-complete"
      placeholder={t('learning_search')}
      {...props}
    />
  );
};

export default SearchLearning;
