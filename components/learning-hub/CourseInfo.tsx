import useDetectTopScreen from '@/hooks/global/useDetectTopScreen';
import { HeartOutlined, ShareAltOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import clsx from 'clsx';
import { Trans, useTranslation } from 'react-i18next';
import { FormattedNumber } from 'react-intl';

interface CourseInfoProps {
  title: string;
  price: number;
  discount: number;
  studentNumber: number;
  time: Date;
}
const CourseInfo = ({
  title,
  price,
  discount,
  studentNumber,
  time,
}: CourseInfoProps) => {
  const { t } = useTranslation('learning');
  const showTitle = useDetectTopScreen('dynamic_title');
  return (
    <div className=" lg:!min-w-[320px] lg:max-w-[440px]">
      <div className="relative hidden w-full h-full space-y-5 lg:block">
        <div className="flex justify-end w-full gap-4 h-7">
          <Tooltip title={t('add_favorite')}>
            <HeartOutlined className="text-xl cursor-pointer" />
          </Tooltip>
          <Tooltip title={t('share_page')}>
            <ShareAltOutlined className="text-xl cursor-pointer" />
          </Tooltip>
        </div>
        <div
          id="dynamic_title"
          className="sticky top-24 flex flex-col w-full gap-3 p-4 bg-white border-[1px] rounded-sm shadow-md"
        >
          <h1
            className={clsx(
              'text-xl font-bold transition-all max-h-0 overflow-hidden duration-300',
              showTitle && 'max-h-20',
            )}
          >
            {title}
          </h1>
          <div className="flex flex-col justify-center gap-2">
            <div className="flex items-center justify-between gap-4">
              <h2 className="font-bold">{t('single_course.price')}</h2>
              <h4
                className={clsx(
                  'flex gap-1 text-sm font-bold',
                  discount && 'line-through font-light',
                )}
              >
                <FormattedNumber value={price} />
                <span>{t('single_course.toman')}</span>
              </h4>
            </div>
            <div className="flex items-center justify-between gap-4">
              <h2 className="font-bold">
                <Trans t={t} i18nKey={'single_course.discount'}>
                  first
                  <span className="text-red-500">
                    {{
                      discount: new Intl.NumberFormat('fa-IR').format(discount),
                    }}
                  </span>
                </Trans>
              </h2>
              <h4 className="flex gap-1 text-sm font-bold text-red-500">
                <FormattedNumber
                  value={discount ? price - price * (discount / 100) : price}
                />
                <span>{t('single_course.toman')}</span>
              </h4>
            </div>
          </div>
          <Button className="!bg-blue-400 !rounded-md !text-white !flex !items-center !justify-center">
            {t('single_course.button')}
          </Button>
          <hr className="border-t-2" />
          <div className="flex flex-col justify-center gap-2">
            <div className="flex items-center justify-between gap-4">
              <h2 className="font-bold">{t('single_course.student_number')}</h2>
              <h4 className="flex gap-1 text-sm">
                <FormattedNumber value={studentNumber} />
                <span>{t('single_course.person')}</span>
              </h4>
            </div>
            <div className="flex items-center justify-between gap-4">
              <h2 className="font-bold">{t('single_course.time')}</h2>
              <h4 className="flex gap-1 text-sm">
                <FormattedNumber value={time.getHours()} />
                <span>{t('single_course.hours')}</span>
                <span>{t('single_course.and')}</span>
                <FormattedNumber value={time.getMinutes()} />
                <span>{t('single_course.minutes')}</span>
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div className="fixed bottom-0 left-0 flex flex-col w-full gap-2 p-4 bg-white lg:hidden">
        <div className="flex items-center justify-between">
          <h2 className="font-bold">{t('single_course.price')}</h2>
          <div className="flex flex-col gap-2">
            <h4
              className={clsx(
                'flex gap-1 text-sm font-bold',
                discount && 'line-through',
              )}
            >
              <FormattedNumber value={price} />
              <span>{t('single_course.toman')}</span>
            </h4>
            <h4 className="flex gap-1 text-sm font-bold text-red-500">
              <FormattedNumber
                value={discount ? price - price * (discount / 100) : price}
              />
              <span>{t('single_course.toman')}</span>
            </h4>
          </div>
        </div>
        <Button className="!w-full !bg-blue-400 !rounded-md !text-white !flex !items-center !justify-center">
          {t('single_course.button')}
        </Button>
      </div>
    </div>
  );
};

export default CourseInfo;
