import SearchLearning from '@/components/learning-hub/SearchLearning';
import { Button, Form } from 'antd';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import paths from '@/utils/paths';

const SearchBanner = () => {
  const { t } = useTranslation('learning');
  const submitHandler = async (values: any) => {
    console.log('value', values);
  };
  return (
    <section className="w-full flex items-center md:min-h-[400px] sm:min-h-[50vh] sm:h-max h-[50vh] bg-[url('/media/images/banner.jpg')] bg-no-repeat bg-cover bg-center">
      <div className="container flex flex-col items-center justify-center gap-3 md:flex-row">
        <Link href={paths.explore}>
          <Button
            type="text"
            className="!bg-green-600 !text-white h-10 rounded-lg shadow-lg"
          >
            {t('all_tutorial')}
          </Button>
        </Link>
        <Form
          className="!w-full !flex justify-center items-center gap-2 md:!w-auto"
          onFinish={submitHandler}
        >
          <div className="flex-grow md:w-[500px]">
            <Form.Item name="search" className="!m-0">
              <SearchLearning />
            </Form.Item>
          </div>
          <Button
            type="text"
            className="!bg-green-600 !text-white !flex !items-center h-10 rounded-full shadow-lg"
            htmlType="submit"
          >
            {t('search')}
          </Button>
        </Form>
      </div>
    </section>
  );
};

export default SearchBanner;
