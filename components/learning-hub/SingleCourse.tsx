import CourseInfo from '@/components/learning-hub/CourseInfo';
import VideoPlayer from '@/components/global/VideoPlayer';

const SingleCourse = () => {
  return (
    <div className="container relative flex justify-between lg:gap-6">
      <div className="w-full overflow-hidden lg:flex-grow">
        <main>
          <h1 className="mb-5 text-2xl font-bold min-h-7 lg:h-7">
            آموزش برنامه نویسی C++ سی پلاس پلاس
          </h1>
          <div className="rounded-lg w-full overflow-hidden relative pt-[56.25%]">
            <VideoPlayer
              videoUrl="http://techslides.com/demos/sample-videos/small.mp4"
              thumbnailUrl="https://drive.google.com/uc?export=view&id=1uSn-dkiiJLG0XhoBdsPFaphBNaJYKARp"
            />
          </div>
          <div className="my-5 space-y-4">
            <h2 className="text-2xl font-bold">
              لورم ایپسوم متن ساختگی با تولید سادگی
            </h2>
            <p className="leading-5 text-justify">
              لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و
              بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح
              گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و
              ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید،
              تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد.
              معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و
              بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند
              که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد
              چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته
              شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه
              رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی
              وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه
              گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به
              پایان برند.
            </p>
          </div>
        </main>
      </div>
      <CourseInfo
        title="آموزش برنامه نویسی C++ سی پلاس پلاس"
        discount={55}
        price={219000}
        studentNumber={30819}
        time={new Date()}
      />
    </div>
  );
};

export default SingleCourse;
