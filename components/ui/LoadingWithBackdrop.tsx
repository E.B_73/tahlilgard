import React from 'react';

const LoadingWithBackdrop = () => {
  return (
    <div className="absolute top-0 z-50 flex items-center justify-center w-full h-full backdrop-blur-[2px]">
      <div className="w-12 h-12 border-4 border-blue-400 rounded-full border-t-transparent animate-spin" />
    </div>
  );
};

export default LoadingWithBackdrop;
