declare global {
  //send check auth data
  interface CheckAuthData {
    userCheckAuthInput: {
      phoneNumber: string;
    };
  }
  // send login data step 1
  interface LoginDataStepOne {
    userLoginInput: {
      recordId: string;
      password: string;
    };
  }
  //send login data step 2
  interface LoginDataStepTwo {
    userSecondLoginInput: {
      recordId: string;
      code: string;
    };
  }
  //send register data
  interface RegisterData {
    userSignupInput: {
      name: string;
      username: string;
      password: string;
      isProvider: boolean;
      verificationCode: string;
      recordId: string;
    };
  }
  //send resend verify code data
  interface ResendCodeData {
    reSendVerifyCodeInput: {
      recordId: string;
    };
  }
  //send forgetPassword data step one
  interface ForgetPasswordDataStepOne {
    forgetPasswordInput: {
      recordId: string;
    };
  }
  //send forgetPassword data step two
  interface ForgetPasswordDataStepTwo {
    forgetPasswordInput: {
      code: number;
      recordId: string;
    };
  }
  //send forgetPassword data step three
  interface ForgetPasswordDataStepThree {
    forgetPasswordStepThreeInput: {
      recordId: string;
      newPassword: string;
    };
  }
}
export default global;
