// import the original type declarations
import 'react-i18next';

// import all namespaces (for the default language, only)
import common from '@/public/locales/fa/common.json';
import learning from '@/public/locales/fa/learning.json';
import liveTrade from '@/public/locales/fa/liveTrade.json';
import analysis from '@/public/locales/fa/analysis.json';

// react-i18next versions higher than 11.11.0
declare module 'react-i18next' {
  // and extend them!
  interface CustomTypeOptions {
    // custom namespace type if you changed it
    defaultNS: 'common';
    // custom resources type
    resources: {
      common: typeof common;
      learning: typeof learning;
      liveTrade: typeof liveTrade;
      analysis: typeof analysis;
    };
  }
}
