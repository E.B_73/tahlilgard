declare global {
  //response for all auth methods
  interface User {
    id: string;
    name: string;
    username: string;
    phoneNumber: string;
    isProvider: boolean;
  }
  interface UserAuth {
    token: string;
    user: User;
    twoFactorAuthenticationRecordId: string;
    twoFactorAuthenticationExpirationTime: string;
    isExist: boolean;
  }
  //check auth
  interface CheckAuthResponse {
    userCheckAuth: {
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
      isExist: boolean;
    };
  }
  //login step 1
  interface LoginResponseStepOne {
    userLogin: {
      token: string;
      user: User;
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //login step 2
  interface LoginResponseStepTwo {
    userSecondLogin: {
      token: string;
      user: User;
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //register
  interface RegisterResponse {
    userSignupAndVerifyPhone: {
      token: string;
      user: User;
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //resend verify code
  interface ResendCodeResponse {
    reSendVerifyCode: {
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //forget-password step one
  interface ForgetPasswordStepOneResponse {
    forgetPassword: {
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //forget-password step two
  interface ForgetPasswordStepTwoResponse {
    forgetPasswordStepTwo: {
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
  //forget-password step three
  interface ForgetPasswordStepThreeResponse {
    forgetPasswordStepThree: {
      token: string;
      user: User;
      twoFactorAuthenticationRecordId: string;
      twoFactorAuthenticationExpirationTime: string;
    };
  }
}
export default global;
