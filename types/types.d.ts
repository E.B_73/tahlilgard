declare global {
  type AuthStep = 'enter_mobile' | 'login' | 'register' | 'send_code';
  type ForgetPassStep = 'neutral' | 'send_code' | 'send_new_pass';
  type CardType = 'analysis' | 'provider';
  type RecordId = string;
  type User = {
    phoneNumber?: string;
    name?: string;
  };
  interface OrderItem {
    id: string;
    title: string;
    seller: string;
    price: number;
    image: string;
  }

  type Cart = {
    orders: OrderItem[];
    totalPrice: number;
  };
  //zustand slices
  type ZustandState = AnalysisSlice &
    ForgetPasswordSlice &
    AuthenticationStepsSlice &
    UserSlice &
    CartSlice;

  // zustand authentication slices type
  interface AuthenticationStepsSlice {
    step: AuthSteps;
    recordId: RecordId;
    countDown: number;
    phoneNumber: string;
    setStep: (step: AuthStep) => void;
    setRecordId: (recordId: RecordId) => void;
    setCountDown: (countDown: number) => void;
    setPhoneNumber: (phoneNumber: string) => void;
  }

  // zustand forget password slices type
  interface ForgetPasswordSlice {
    forgetPassStep: ForgetPassStep;
    forgetPassRecordId: RecordId;
    forgetPassCountDown: number;
    forgetPassSetStep: (step: ForgetPassStep) => void;
    forgetPassSetRecordId: (recordId: RecordId) => void;
    forgetPassSetCountDown: (countDown: number) => void;
  }
  // zustand user slices type
  interface UserSlice {
    token: string | null;
    user: User | null;
    login: ({ token: string, user: User }) => void;
    logout: () => void;
  }
  // zustand analysis slices type
  interface AnalysisSlice {
    cardType: CardType;
    setCardType: (type: CardType) => void;
  }
  // zustand cart slices type
  interface CartSlice {
    showCart: boolean;
    cart: Cart;
    setShowCart: (show: boolean) => void;
    addOrder: (order: OrderItem) => void;
    removeOrder: (orderId: string) => void;
    clearCart: () => void;
  }
}
export default global;
