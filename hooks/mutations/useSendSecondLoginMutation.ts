import { MUTATION_LOGIN_STEP_TWO } from '@/graphql/mutations';
import successHandler from '@/utils/toast-handler/SuccessHandler';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import router from 'next/router';
// zustand sotre
import useAppStore from '@/store/store';
import paths from '@/utils/paths';

const useSendSecondLoginMutation = () => {
  const { t } = useTranslation('common');
  const login = useAppStore((state) => state.login);
  // mutation
  const [mutate, options] = useMutation<LoginResponseStepTwo, LoginDataStepTwo>(
    MUTATION_LOGIN_STEP_TWO,
    {
      onCompleted(data) {
        if (
          data &&
          !_.isEmpty(data) &&
          data.userSecondLogin &&
          !data.userSecondLogin.token
        ) {
          successHandler(t('Success' as any));
          login({
            token: data.userSecondLogin.token,
            user: {
              phoneNumber: data.userSecondLogin.user.phoneNumber,
              name: data.userSecondLogin.user.name,
            },
          });
          router.replace(paths.home);
        }
      },
      onError(error) {
        error.graphQLErrors.map((e: GraphQLError) => {
          errorHandler(t(e.message as any));
        });
      },
    },
  );

  return {
    sendCode: mutate,
    ...options,
  };
};

export default useSendSecondLoginMutation;
