import { MUTATION_REGISTER } from '@/graphql/mutations';
import successHandler from '@/utils/toast-handler/SuccessHandler';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import router from 'next/router';
// zustand store
import useAppStore from '@/store/store';
import paths from '@/utils/paths';

const useSendRegisterMutation = () => {
  const { t } = useTranslation('common');
  const login = useAppStore((state) => state.login);
  // mutation
  const [mutate, options] = useMutation<RegisterResponse, RegisterData>(
    MUTATION_REGISTER,
    {
      onCompleted(data) {
        if (
          data &&
          !_.isEmpty(data) &&
          data.userSignupAndVerifyPhone &&
          data.userSignupAndVerifyPhone.token
        ) {
          successHandler(t('Success' as any));
          login({
            token: data.userSignupAndVerifyPhone.token,
            user: {
              phoneNumber: data.userSignupAndVerifyPhone.user.phoneNumber,
              name: data.userSignupAndVerifyPhone.user.name,
            },
          });
          router.replace(paths.home);
        }
      },
      onError(error) {
        error.graphQLErrors.map((e: GraphQLError) => {
          errorHandler(t(e.message as any));
        });
      },
    },
  );

  return {
    sendRegister: mutate,
    ...options,
  };
};

export default useSendRegisterMutation;
