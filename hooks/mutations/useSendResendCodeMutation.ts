import { MUTATION_RESEND_CODE } from '@/graphql/mutations';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import { calculateExpirationTime } from '@/utils/helper';

const useSendResendCodeMutation = (form: 'forget_pass_form' | 'auth_form') => {
  const { t } = useTranslation('common');
  const {
    setCountDown,
    setRecordId,
    setForgetPassCountDown,
    setForgetPassRecordId,
  } = useAppStore(
    (state) => ({
      setCountDown: state.setCountDown,
      setRecordId: state.setRecordId,
      setForgetPassCountDown: state.forgetPassSetCountDown,
      setForgetPassRecordId: state.forgetPassSetRecordId,
    }),
    shallow,
  );
  // mutation
  const [sendResendCode, options] = useMutation<
    ResendCodeResponse,
    ResendCodeData
  >(MUTATION_RESEND_CODE, {
    onCompleted(data) {
      if (data && !_.isEmpty(data) && data.reSendVerifyCode) {
        if (form === 'auth_form') {
          setRecordId(data.reSendVerifyCode.twoFactorAuthenticationRecordId);
          const expirationTime = calculateExpirationTime(
            +data.reSendVerifyCode.twoFactorAuthenticationExpirationTime,
          );
          setCountDown(expirationTime);
        } else if (form === 'forget_pass_form') {
          setForgetPassRecordId(
            data.reSendVerifyCode.twoFactorAuthenticationRecordId,
          );
          setForgetPassCountDown(
            Math.floor(
              (+data.reSendVerifyCode.twoFactorAuthenticationExpirationTime -
                Date.now()) /
                1000,
            ),
          );
        }
      }
    },
    onError(error) {
      error.graphQLErrors.map((e: GraphQLError) => {
        errorHandler(t(e.message as any));
      });
    },
  });

  return {
    sendResendCode,
    ...options,
  };
};

export default useSendResendCodeMutation;
