import { MUTATION_FORGET_PASSWORD_STEP_TWO } from '@/graphql/mutations';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';

const useSendSecondForgetPassMutation = () => {
  const { t } = useTranslation('common');
  const { setStep, setRecordId } = useAppStore(
    (state) => ({
      setStep: state.forgetPassSetStep,
      setRecordId: state.forgetPassSetRecordId,
    }),
    shallow,
  );
  // mutation
  const [sendPass, options] = useMutation<
    ForgetPasswordStepTwoResponse,
    ForgetPasswordDataStepTwo
  >(MUTATION_FORGET_PASSWORD_STEP_TWO, {
    onCompleted(data) {
      if (data && !_.isEmpty(data) && data.forgetPasswordStepTwo) {
        setStep('send_new_pass');
        setRecordId(data.forgetPasswordStepTwo.twoFactorAuthenticationRecordId);
      }
    },
    onError(error) {
      error.graphQLErrors.map((e: GraphQLError) => {
        errorHandler(t(e.message as any));
      });
    },
  });

  return {
    sendPass,
    ...options,
  };
};

export default useSendSecondForgetPassMutation;
