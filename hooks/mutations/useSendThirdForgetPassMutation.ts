import { MUTATION_FORGET_PASSWORD_STEP_THREE } from '@/graphql/mutations';
import successHandler from '@/utils/toast-handler/SuccessHandler';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import router from 'next/router';
import paths from '@/utils/paths';

const useSendThirdForgetPassMutation = () => {
  const { t } = useTranslation('common');
  const login = useAppStore((state) => state.login);
  // mutation
  const [sendPass, options] = useMutation<
    ForgetPasswordStepThreeResponse,
    ForgetPasswordDataStepThree
  >(MUTATION_FORGET_PASSWORD_STEP_THREE, {
    onCompleted(data) {
      if (data && !_.isEmpty(data) && data.forgetPasswordStepThree) {
        if (
          data.forgetPasswordStepThree.token &&
          data.forgetPasswordStepThree.user
        ) {
          successHandler(t('Success' as any));
          login({
            token: data.forgetPasswordStepThree.token,
            user: {
              phoneNumber: data.forgetPasswordStepThree.user.phoneNumber,
              name: data.forgetPasswordStepThree.user.name,
            },
          });
          router.replace(paths.home);
        }
      }
    },
    onError(error) {
      error.graphQLErrors.map((e: GraphQLError) => {
        errorHandler(t(e.message as any));
      });
    },
  });

  return {
    sendPass,
    ...options,
  };
};

export default useSendThirdForgetPassMutation;
