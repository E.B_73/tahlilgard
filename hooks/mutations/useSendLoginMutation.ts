import { MUTATION_LOGIN_STEP_ONE } from '@/graphql/mutations';
import successHandler from '@/utils/toast-handler/SuccessHandler';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import router from 'next/router';
import { calculateExpirationTime } from '@/utils/helper';
import paths from '@/utils/paths';

const useSendLoginMutation = () => {
  const { t } = useTranslation('common');
  const { setStep, setRecordId, setCountDown, login } = useAppStore(
    (state) => ({
      setStep: state.setStep,
      setRecordId: state.setRecordId,
      setCountDown: state.setCountDown,
      login: state.login,
    }),
    shallow,
  );
  // mutation
  const [mutate, options] = useMutation<LoginResponseStepOne, LoginDataStepOne>(
    MUTATION_LOGIN_STEP_ONE,
    {
      onCompleted(data) {
        if (data && !_.isEmpty(data) && data.userLogin) {
          if (!data.userLogin.token) {
            setStep('send_code');
            setRecordId(data.userLogin.twoFactorAuthenticationRecordId);
            const expirationTime = calculateExpirationTime(
              +data.userLogin.twoFactorAuthenticationExpirationTime,
            );
            setCountDown(expirationTime);
          } else {
            successHandler(t('Success' as any));
            login({
              token: data.userLogin.token,
              user: {
                phoneNumber: data.userLogin.user.phoneNumber,
                name: data.userLogin.user.name,
              },
            });
            router.replace(paths.home);
          }
        }
      },
      onError(error) {
        error.graphQLErrors.map((e: GraphQLError) => {
          errorHandler(t(e.message as any));
        });
      },
    },
  );

  return {
    sendLogin: mutate,
    ...options,
  };
};

export default useSendLoginMutation;
