import { MUTATION_CHECK_AUTH } from '@/graphql/mutations';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import { calculateExpirationTime } from '@/utils/helper';

const useSendCheckAuthMutation = () => {
  const { t } = useTranslation('common');
  const { setStep, setRecordId, setCountDown, setPhoneNumber } = useAppStore(
    (state) => ({
      setStep: state.setStep,
      setRecordId: state.setRecordId,
      setCountDown: state.setCountDown,
      setPhoneNumber: state.setPhoneNumber,
    }),
    shallow,
  );
  // mutation
  const [mutate, options] = useMutation<CheckAuthResponse, CheckAuthData>(
    MUTATION_CHECK_AUTH,
    {
      onCompleted(data, arg) {
        if (data && !_.isEmpty(data) && data.userCheckAuth) {
          if (data.userCheckAuth.isExist) {
            setStep('login');
            setPhoneNumber(arg?.variables?.userCheckAuthInput?.phoneNumber);
          } else {
            setStep('register');
          }
          setRecordId(data.userCheckAuth.twoFactorAuthenticationRecordId);
          const expirationTime = calculateExpirationTime(
            +data.userCheckAuth.twoFactorAuthenticationExpirationTime,
          );
          setCountDown(expirationTime);
        }
      },
      onError(error) {
        error.graphQLErrors.map((e: GraphQLError) => {
          errorHandler(t(e.message as any));
        });
      },
    },
  );

  return {
    sendCheckAuth: mutate,
    ...options,
  };
};

export default useSendCheckAuthMutation;
