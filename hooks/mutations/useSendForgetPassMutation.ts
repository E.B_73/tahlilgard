import { MUTATION_FORGET_PASSWORD_STEP_ONE } from '@/graphql/mutations';
import errorHandler from '@/utils/toast-handler/ErrorHandler';
import { useMutation } from '@apollo/client';
import { GraphQLError } from 'graphql/error/GraphQLError';
import _ from 'lodash';
import { useTranslation } from 'next-i18next';
import useAppStore from '@/store/store';
import shallow from 'zustand/shallow';
import router from 'next/router';
import { calculateExpirationTime } from '@/utils/helper';
import paths from '@/utils/paths';

const useSendForgetPassMutation = () => {
  const { t } = useTranslation('common');
  const { setRecordId, setCountDown, setStep } = useAppStore(
    (state) => ({
      setRecordId: state.forgetPassSetRecordId,
      setCountDown: state.forgetPassSetCountDown,
      setStep: state.forgetPassSetStep,
    }),
    shallow,
  );
  // mutation
  const [sendPass, options] = useMutation<
    ForgetPasswordStepOneResponse,
    ForgetPasswordDataStepOne
  >(MUTATION_FORGET_PASSWORD_STEP_ONE, {
    onCompleted(data) {
      if (data && !_.isEmpty(data) && data.forgetPassword) {
        const expirationTime = calculateExpirationTime(
          +data.forgetPassword.twoFactorAuthenticationExpirationTime,
        );
        setRecordId(data.forgetPassword.twoFactorAuthenticationRecordId);
        setStep('send_code');
        setCountDown(expirationTime);
        router.replace(paths.forget_password);
      }
    },
    onError(error) {
      error.graphQLErrors.map((e: GraphQLError) => {
        errorHandler(t(e.message as any));
      });
    },
  });

  return {
    sendPass,
    ...options,
  };
};

export default useSendForgetPassMutation;
