import checkExpireToken from '@/utils/auth/ExpireToken';
import paths from '@/utils/paths';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

const useDetectUser = (redirect = false) => {
  const router = useRouter();
  const [loading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    if (typeof window !== 'undefined') {
      const token = localStorage.getItem('token');
      if (token && !checkExpireToken(token)) {
        if (redirect) {
          router.replace(paths.home);
        }
      } else {
        setLoading(false);
        localStorage.removeItem('token');
      }
    }
  }, []);
  return { loading };
};

export default useDetectUser;
