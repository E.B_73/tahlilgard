import { useTranslation } from 'next-i18next';

const useHeaderMenuItems = () => {
  const { t } = useTranslation('common');
  return [
    {
      title: t('Tutorial'),
      link: '/explore',
      icon: 'AiOutlineEdit',
    },
    {
      title: t('Tutorials'),
      link: '/learning',
      icon: 'AiOutlineRead',
    },
    {
      title: t('Analysis Forum'),
      link: '/analysis-forum',
      icon: 'AiOutlineLineChart',
    },
    {
      title: t('live-trade'),
      link: '/live-trade',
      icon: 'AiOutlineFundProjectionScreen',
    },
  ];
};

export default useHeaderMenuItems;
