import { useEffect, useState, useCallback } from 'react';

// ? a simple Custom Hook for check token has exist or not.
export default function useToken() {
  const [token, setToken] = useState<string | null>(null);
  const onChangeToken = useCallback(async () => {
    if (typeof window !== 'undefined') setToken(localStorage.getItem('token'));
  }, []);

  useEffect(() => {
    // ? listener for changing localstorage
    if (typeof window !== 'undefined')
      window.addEventListener('storage', onChangeToken);

    // ? clear listener after unmount
    return () => {
      if (typeof window !== 'undefined')
        window.removeEventListener('storage', onChangeToken);
    };
  }, [onChangeToken]);

  return token;
}
