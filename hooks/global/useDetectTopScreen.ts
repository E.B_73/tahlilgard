import { useEffect, useState } from 'react';

const useDetectTopScreen = (id: string) => {
  const [equalTop, setEqualTop] = useState(false);
  useEffect(() => {
    const setPosition = (boxPosition: DOMRect) => {
      if (window.pageYOffset >= boxPosition.top) {
        setEqualTop(true);
      } else {
        setEqualTop(false);
      }
    };
    if (id) {
      const box = document.getElementById(id)! as HTMLElement;
      const boxPosition = box.getBoundingClientRect();
      const fn = setPosition.bind(null, boxPosition);
      window.addEventListener('scroll', fn);
      return () => window.removeEventListener('scroll', fn);
    }
  }, [setEqualTop]);
  return equalTop;
};

export default useDetectTopScreen;
