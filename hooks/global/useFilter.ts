import { useTranslation } from 'next-i18next';

export const useLearningCategoryOptions = () => {
  const { t } = useTranslation('common', { keyPrefix: 'sidebar.category' });
  const checkboxOptions = [
    { label: t('programing'), value: t('programing') },
    { label: t('machine_engineering'), value: t('machine_engineering') },
    { label: t('civil_engineering'), value: t('civil_engineering') },
    {
      label: t('electrical_engineering'),
      value: t('electrical_engineering'),
    },
  ];

  return checkboxOptions;
};

export const useLiveTradeCategoryOptions = () => {
  const { t } = useTranslation('liveTrade', { keyPrefix: 'sidebar.category' });
  const checkboxOptions = [
    { label: t('finished'), value: t('finished') },
    { label: t('on_air'), value: t('on_air') },
    { label: t('future'), value: t('future') },
  ];

  return checkboxOptions;
};

export const useEducationTypeOptions = () => {
  const { t } = useTranslation('common', {
    keyPrefix: 'sidebar.type_of_education',
  });
  const checkboxOptions = [
    { label: t('all'), value: t('all') },
    { label: t('offline_video'), value: t('offline_video') },
    { label: t('online_webinar'), value: t('online_webinar') },
  ];

  return checkboxOptions;
};

export const usePriceOptions = () => {
  const { t } = useTranslation('common', {
    keyPrefix: 'sidebar.price',
  });
  const checkboxOptions = [
    { label: t('all'), value: t('all') },
    { label: t('free'), value: t('free') },
    { label: t('non_free'), value: t('non_free') },
  ];

  return checkboxOptions;
};
