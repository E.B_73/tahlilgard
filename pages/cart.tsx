import { getLayout } from '@/components/layouts/HomeLayout';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import CartContainer from '@/components/cart/CartContainer';

const Cart = () => {
  const { t } = useTranslation('common', { keyPrefix: 'cart' });

  return (
    <>
      <Head>
        <title>{t('page_title')}</title>
      </Head>
      <CartContainer />
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}
// export layout
Cart.getLayout = getLayout;

export default Cart;
