import Container from '@/components/authentication/Container';
import { getLayout } from '@/components/layouts/AuthLayout';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';

const AuthenticationPage = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('Login to Tahlilgard')}</title>
      </Head>
      <Container />
    </>
  );
};

export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

AuthenticationPage.getLayout = getLayout;

export default AuthenticationPage;
