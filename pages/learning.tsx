import { getLayout } from '@/components/layouts/HomeLayout';
import PackagesSlider from '@/components/learning-hub/PackagesSlider';
import SearchBanner from '@/components/learning-hub/SearchBanner';
import SubjectsCategory from '@/components/learning-hub/SubjectsCategory';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';

const Learning = () => {
  const { t } = useTranslation();
  const { t: tl } = useTranslation('learning');
  return (
    <>
      <Head>
        <title>{t('Tutorial')}</title>
      </Head>
      <div className="w-full bg-white">
        <div className="overflow-hidden">
          <SearchBanner />
          <SubjectsCategory />
          <PackagesSlider
            title={tl('most_visited')}
            slides={[
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
            ]}
          />
          <PackagesSlider
            title={tl('most_popular')}
            slides={[
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
            ]}
          />
          <PackagesSlider
            title={tl('most_favorite')}
            slides={[
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
              {
                imgUrl: '/media/images/banner.jpg',
                link: '/courses/1',
                title: 'الب',
                price: 6500,
              },
            ]}
          />
        </div>
      </div>
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'learning'])),
    },
  };
}

// export layout
Learning.getLayout = getLayout;

export default Learning;
