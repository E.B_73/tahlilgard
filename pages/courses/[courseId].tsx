import { getLayout } from '@/components/layouts/HomeLayout';
import SingleCourse from '@/components/learning-hub/SingleCourse';
import { GetServerSidePropsContext, InferGetServerSidePropsType } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';

const SingleCoursePage = ({ query }: SingleCoursePageProps) => {
  const { t } = useTranslation('common');
  console.log(query);
  return (
    <>
      <Head>
        <title>{t('Tutorial')}</title>
      </Head>
      <div className="relative w-full py-4 bg-white">
        <SingleCourse />
      </div>
    </>
  );
};

export async function getServerSideProps({
  query,
  locale,
}: GetServerSidePropsContext) {
  const { courseId } = query;
  return {
    props: {
      query: courseId,
      ...(await serverSideTranslations(locale as string, [
        'common',
        'learning',
      ])),
    },
  };
}
export type SingleCoursePageProps = InferGetServerSidePropsType<
  typeof getServerSideProps
>;

// export layout
SingleCoursePage.getLayout = getLayout;

export default SingleCoursePage;
