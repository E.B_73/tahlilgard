import Heatmap from '@/components/analysis-forum/heatmaps/Heatmap';
import Cards from '@/components/analysis-forum/cards/Cards';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import { getLayout } from '@/components/layouts/HomeLayout';
import SearchPackages from '@/components/analysis-forum/SearchPackages';
import CategoryBar from '@/components/analysis-forum/CategoryBar';

const AnalysisForum = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('Analysis Forum')}</title>
      </Head>
      <div className="bg-white">
        <div className="container">
          <Heatmap />
          <CategoryBar />
          <SearchPackages />
          <Cards />
        </div>
      </div>
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'analysis'])),
    },
  };
}

// export layout
AnalysisForum.getLayout = getLayout;

export default AnalysisForum;
