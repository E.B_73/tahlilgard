import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Contents from '@/components/rules/Contents';
import { getLayout } from '@/components/layouts/HomeLayout';
import Head from 'next/head';
const Rules = () => {
  const { t } = useTranslation();

  return (
    <>
      <Head>
        <title>{t('Terms and Conditions')}</title>
      </Head>
      <div>
        <Contents />
      </div>
    </>
  );
};
// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

Rules.getLayout = getLayout;

export default Rules;
