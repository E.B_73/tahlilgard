// components
import Container from '@/components/forget-password/Container';
import { getLayout } from '@/components/layouts/AuthLayout';
// hooks
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
const ForgetPasswordPage = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('Forget Password')}</title>
      </Head>
      <Container />
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}

ForgetPasswordPage.getLayout = getLayout;

export default ForgetPasswordPage;
