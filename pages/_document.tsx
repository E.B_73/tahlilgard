import { DEFAULT_LOCALE } from '@/utils/constants';
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document';
import { getLangDir } from 'rtl-detect';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }
  render() {
    const { locale = DEFAULT_LOCALE } = this.props;
    return (
      <Html dir={getLangDir(locale)}>
        <Head>
          <link rel="icon" href="/media/favicon.ico" />
        </Head>
        <body>
          <Main />
          <div id="portal" />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
