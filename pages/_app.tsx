import '@/styles/css/globals.css';
import '@/styles/css/tailwind.css';
import '@/styles/css/antd.css';
import '@/styles/sass/main.scss';

import type { AppProps } from 'next/app';
import { appWithTranslation } from 'next-i18next';
import { ConfigProvider } from 'antd';
import { useRouter } from 'next/router';
import { ApolloProvider } from '@apollo/client';
import client from '../apollo-client';
import { NextPage } from 'next';
import { ReactElement, ReactNode } from 'react';
import { IntlProvider } from 'react-intl';
import { DEFAULT_LOCALE } from '@/utils/constants';

export type NextPageWithLayout = NextPage & {
  // eslint-disable-next-line
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout || ((page: ReactElement) => page);
  const { locale = DEFAULT_LOCALE } = useRouter();
  return (
    <IntlProvider locale={locale} defaultLocale={DEFAULT_LOCALE}>
      <ApolloProvider client={client}>
        <ConfigProvider
          direction={locale == 'fa' ? 'rtl' : 'ltr'}
          theme={{
            token: {
              fontFamily: 'Vazir',
            },
          }}
        >
          {getLayout(<Component {...pageProps} />)}
        </ConfigProvider>
      </ApolloProvider>
    </IntlProvider>
  );
}

export default appWithTranslation(MyApp);
