// React
import SearchBar from '@/components/analysis-forum/SearchBar';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import { getLayout } from '@/components/layouts/LearningLayout';

const Explore = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('Tutorial')}</title>
      </Head>
      <div className="w-full bg-white">
        <div className={'w-full'}>
          <SearchBar />
        </div>
      </div>
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'learning'])),
    },
  };
}

// export layout
Explore.getLayout = getLayout;

export default Explore;
