import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import { getLayout } from '@/components/layouts/HomeLayout';
import PackageList from '@/components/live-trade/PackageList';
import LiveTradeFilter from '@/components/filters/LiveTradeFilter';
import SortBar from '@/components/layouts/SortBar';
import InfoBanner from '@/components/live-trade/InfoBanner';
import SortForm from '@/components/filters/SortForm';
import SortFormModal from '@/components/filters/SortFormModal';
// import HowToCreateLiveTrade from '@/components/live-trade/HowToCreateLiveTrade';
// import HowToUseLiveTrade from '@/components/live-trade/HowToUseLiveTrade';

const LiveTrade = () => {
  const { t } = useTranslation('common');
  const sortOptions = t('sortbar.live_trade', { returnObjects: true });
  return (
    <>
      <Head>
        <title>{t('live-trade')}</title>
      </Head>
      <InfoBanner />
      <div className="container flex bg-white">
        <aside className="relative hidden w-60 lg:block">
          <LiveTradeFilter />
        </aside>
        <div className="flex-1">
          <SortBar
            sortForm={<SortForm options={sortOptions} />}
            sortFormModal={<SortFormModal options={sortOptions} />}
            filterModalContent={<LiveTradeFilter />}
          />
          <main className="w-full p-4">
            <PackageList />
          </main>
        </div>
      </div>
      {/* <HowToCreateLiveTrade />
      <HowToUseLiveTrade /> */}
    </>
  );
};

// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'liveTrade',
        'learning',
      ])),
    },
  };
}

// export layout
LiveTrade.getLayout = getLayout;

export default LiveTrade;
