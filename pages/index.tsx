import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import TraderSection from '@/components/landing/TraderSection';
import WhyUsSection from '@/components/landing/WhyUsSection';
import LoginSection from '@/components/landing/LoginSection';
import { getLayout } from '@/components/layouts/HomeLayout';

const Home = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('Tahlilgard')}</title>
      </Head>
      <TraderSection />
      <WhyUsSection />
      <LoginSection />
    </>
  );
};
// Function to Translate
export async function getStaticProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
    },
  };
}
// export layout
Home.getLayout = getLayout;

export default Home;
