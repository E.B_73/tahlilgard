import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import { useTranslation } from 'next-i18next';
import { getLayout } from '@/components/layouts/HomeLayout';
import LoadingProfile from '@/components/analysis-forum/provider-profile/LoadingProfile';
import dynamic from 'next/dynamic';

const ProviderAllInfo = dynamic(
  () => import('@/components/analysis-forum/provider-profile/ProviderAllInfo'),
  {
    loading: () => <LoadingProfile />,
    ssr: false,
  },
);

const LiveTrade = () => {
  const { t } = useTranslation();
  return (
    <>
      <Head>
        <title>{t('live-trade')}</title>
      </Head>
      <div className="space-y-10">
        <ProviderAllInfo />
      </div>
    </>
  );
};

// Function to Translate
export async function getServerSideProps({ locale }: { locale: string }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'analysis'])),
    },
  };
}

// export layout
LiveTrade.getLayout = getLayout;

export default LiveTrade;
