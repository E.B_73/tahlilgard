import {
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  ApolloLink,
  DefaultOptions,
} from '@apollo/client';
const graphQLServerHttpLink = createHttpLink({
  uri: process.env.NEXT_PUBLIC_GRAPHQL_URL,
});
const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    fetchPolicy: 'network-only',
  },
};
const authLink = new ApolloLink((operation, forward) => {
  // get the authentication token from local storage if it exists
  let token: string | undefined | null = undefined;
  if (typeof window !== 'undefined') {
    token = localStorage.getItem('token');
  }
  operation.setContext(({ headers }: { headers: any }) => {
    if (token) {
      return {
        headers: {
          ...headers,
          authorization: `Bearer ${token}`,
        },
      };
    } else {
      return {
        headers: {
          ...headers,
        },
      };
    }
  });
  const { operationName } = operation;
  return forward(operation).map((response) => {
    // check if there is token in response
    // check token
    if (
      response?.data &&
      response?.data[operationName] &&
      response?.data[operationName]?.token
    ) {
      localStorage.setItem('token', response.data[operationName].token);
    }
    return response;
  });
});

const client = new ApolloClient({
  link: ApolloLink.from([authLink, graphQLServerHttpLink]),
  cache: new InMemoryCache(),
  defaultOptions: defaultOptions,
});

export default client;
