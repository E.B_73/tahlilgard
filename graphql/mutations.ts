import { DocumentNode, gql } from '@apollo/client';

export const MUTATION_CHECK_AUTH: DocumentNode = gql`
  mutation userCheckAuth($userCheckAuthInput: userCheckAuthInput!) {
    userCheckAuth(userCheckAuthInput: $userCheckAuthInput) {
      isExist
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_LOGIN_STEP_ONE: DocumentNode = gql`
  mutation userLogin($userLoginInput: userLoginInput!) {
    userLogin(userLoginInput: $userLoginInput) {
      token
      user {
        phoneNumber
        name
      }
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_LOGIN_STEP_TWO: DocumentNode = gql`
  mutation userSecondLogin($userSecondLoginInput: userSecondLoginInput!) {
    userSecondLogin(userSecondLoginInput: $userSecondLoginInput) {
      token
      user {
        phoneNumber
        name
      }
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_REGISTER: DocumentNode = gql`
  mutation userSignupAndVerifyPhone($userSignupInput: userSignupInput!) {
    userSignupAndVerifyPhone(userSignupInput: $userSignupInput) {
      token
      user {
        phoneNumber
        name
      }
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_RESEND_CODE: DocumentNode = gql`
  mutation reSendVerifyCode(
    $reSendVerifyCodeInput: userReSendVerifyCodeInput!
  ) {
    reSendVerifyCode(reSendVerifyCodeInput: $reSendVerifyCodeInput) {
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_FORGET_PASSWORD_STEP_ONE: DocumentNode = gql`
  mutation forgetPassword($forgetPasswordInput: userForgetPassInput!) {
    forgetPassword(forgetPasswordInput: $forgetPasswordInput) {
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_FORGET_PASSWORD_STEP_TWO: DocumentNode = gql`
  mutation forgetPasswordStepTwo(
    $forgetPasswordInput: userForgetPassStepTwoInput!
  ) {
    forgetPasswordStepTwo(forgetPasswordInput: $forgetPasswordInput) {
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const MUTATION_FORGET_PASSWORD_STEP_THREE: DocumentNode = gql`
  mutation forgetPasswordStepThree(
    $forgetPasswordStepThreeInput: userForgetPassStepThreeInput!
  ) {
    forgetPasswordStepThree(
      forgetPasswordStepThreeInput: $forgetPasswordStepThreeInput
    ) {
      token
      user {
        phoneNumber
        name
      }
      twoFactorAuthenticationRecordId
      twoFactorAuthenticationExpirationTime
    }
  }
`;

export const GET_BOURSE_HEATMAP = gql`
  mutation getBoorsHeatmap {
    getBoorsHeatmap {
      heatmap
    }
  }
`;
export const GET_CRYPTO_HEATMAP = gql`
  mutation getCryptoHeatmap {
    getCryptoHeatmap {
      heatmap
    }
  }
`;
