import { OperationVariables, TypedDocumentNode } from '@apollo/client';
import { DocumentNode } from 'graphql';
import client from '../apollo-client';

const sendQuery = async (
  query: DocumentNode | TypedDocumentNode<any, OperationVariables>,
) => {
  const res = await client.query({ query });
  return res;
};

export default sendQuery;
