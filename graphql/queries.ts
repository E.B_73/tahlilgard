import { DocumentNode, gql } from '@apollo/client';

export const GET_ALL_CURRENCIES: DocumentNode = gql`
  query getAllCryptoCoins {
    getAllCryptoCoins {
      symbol
      name
      id
    }
  }
`;

export const GET_BOURSE_HEATMAP = gql`
  query getBoorsHeatmap {
    getBoorsHeatmap {
      heatmap
    }
  }
`;
export const GET_CRYPTO_HEATMAP = gql`
  query getCryptoHeatmap {
    getCryptoHeatmap {
      heatmap
    }
  }
`;
