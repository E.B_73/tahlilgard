import { StateCreator } from 'zustand';
import produce from 'immer';

export const createAnalysisSlice: StateCreator<
  ZustandState,
  [],
  [],
  AnalysisSlice
> = (set) => ({
  cardType: 'analysis',
  setCardType: (type) =>
    set(
      produce((state: ZustandState) => {
        state.cardType = type;
      }),
    ),
});
