import { StateCreator } from 'zustand';
import produce from 'immer';

export const createForgetPasswordSlice: StateCreator<
  ZustandState,
  [],
  [],
  ForgetPasswordSlice
> = (set) => ({
  forgetPassStep: 'neutral',
  forgetPassRecordId: '',
  forgetPassCountDown: 0,
  forgetPassSetStep: (step) =>
    set(
      produce((state) => {
        state.forgetPassStep = step;
      }),
    ),
  forgetPassSetRecordId: (recordId) =>
    set(
      produce((state) => {
        state.forgetPassRecordId = recordId;
      }),
    ),
  forgetPassSetCountDown: (countDown) =>
    set(
      produce((state) => {
        state.forgetPassCountDown = countDown;
      }),
    ),
});
