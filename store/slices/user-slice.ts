import { StateCreator } from 'zustand';
import produce from 'immer';
import { IS_SERVER } from '@/utils/constants';

const detectToken = () => {
  if (!IS_SERVER) {
    return localStorage.getItem('token') as string;
  }
};

export const createUserSlice: StateCreator<ZustandState, [], [], UserSlice> = (
  set,
) => ({
  token: detectToken() ?? null,
  user: null,
  login: ({ token, user }) =>
    set(
      produce((state) => {
        state.token = token;
        state.user = user;
        if (!IS_SERVER) localStorage.setItem('token', token);
      }),
    ),
  logout: () =>
    set(
      produce((state) => {
        state.token = null;
        state.user = null;
        localStorage.removeItem('token');
      }),
    ),
});
