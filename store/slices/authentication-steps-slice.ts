import { StateCreator } from 'zustand';
import produce from 'immer';

export const createAuthenticationStepsSlice: StateCreator<
  ZustandState,
  [],
  [],
  AuthenticationStepsSlice
> = (set) => ({
  step: 'enter_mobile',
  recordId: '',
  countDown: 0,
  phoneNumber: '',
  setStep: (step) =>
    set(
      produce((state) => {
        state.step = step;
      }),
    ),
  setRecordId: (recordId) =>
    set(
      produce((state) => {
        state.recordId = recordId;
      }),
    ),
  setCountDown: (countDown) =>
    set(
      produce((state) => {
        state.countDown = countDown;
      }),
    ),
  setPhoneNumber: (phoneNumber) =>
    set(
      produce((state) => {
        state.phoneNumber = phoneNumber;
      }),
    ),
});
