import { StateCreator } from 'zustand';
import produce from 'immer';

export const createCartSlice: StateCreator<ZustandState, [], [], CartSlice> = (
  set,
) => ({
  showCart: false,
  cart: { orders: [], totalPrice: 0 },
  setShowCart: (type) =>
    set(
      produce((state: ZustandState) => {
        state.showCart = type;
      }),
    ),
  addOrder: (order) =>
    set(
      produce((state: ZustandState) => {
        if (state.cart.orders) {
          const orderItem = state.cart.orders.find(
            (item) => order.id === item.id,
          );
          if (!orderItem) {
            state.cart.orders.push(order);
            state.cart.totalPrice += order.price;
          }
        }
      }),
    ),
  removeOrder: (orderId) =>
    set(
      produce((state: ZustandState) => {
        if (orderId && state.cart.orders) {
          const orderItem = state.cart.orders.find(
            (item) => orderId === item.id,
          );
          if (orderItem && orderItem.id) {
            state.cart.orders = state.cart.orders.filter(
              (item) => item.id !== orderItem?.id,
            );
            state.cart.totalPrice -= orderItem.price;
          }
        }
      }),
    ),
  clearCart: () =>
    set(
      produce((state: ZustandState) => {
        state.cart = { orders: [], totalPrice: 0 };
      }),
    ),
});
