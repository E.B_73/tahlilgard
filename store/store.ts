import { createForgetPasswordSlice } from '@/store/slices/forget-password-slice';
import { createAuthenticationStepsSlice } from '@/store/slices/authentication-steps-slice';
import { createAnalysisSlice } from '@/store/slices/analysis-slice';
import { createCartSlice } from '@/store/slices/cart-slice';
import { create } from 'zustand';
import { createUserSlice } from '@/store/slices/user-slice';

const useAppStore = create<ZustandState>()((...a) => ({
  ...createAuthenticationStepsSlice(...a),
  ...createUserSlice(...a),
  ...createForgetPasswordSlice(...a),
  ...createAnalysisSlice(...a),
  ...createCartSlice(...a),
}));
export default useAppStore;
