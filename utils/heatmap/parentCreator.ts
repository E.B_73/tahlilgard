export const parentCreator = (data: string[][]) => {
  const newData = [
    ...data.map((item) => [
      item[0].toUpperCase() + ' ' + `${+item[3] > 0 ? '⬆ +' : '⬇ '}${item[3]}`,
      item[1],
      +item[2],
      +item[3] > 5 ? 5 : +item[3] < -5 ? -5 : +item[3],
    ]),
  ];

  const CONSTDATA = [
    [
      'Currency',
      'Parent',
      'Market trade volume (size)',
      'Market increase/decrease (color)',
    ],
    ['بازار', null, 0, 0],
  ];

  const parents = [...new Set(newData.map((item) => item[1]))].map((item) => [
    item,
    'بازار',
    0,
    0,
  ]);
  return [...CONSTDATA, ...parents, ...newData.slice(0, 750)];
};
