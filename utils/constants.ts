export const DEFAULT_LOCALE = process.env.NEXT_PUBLIC_DEFAULT_LOCALE as string;
export const PANEL_LINK = process.env.NEXT_PUBLIC_PANEL as string;
export const ORDER_HISTORY = `${process.env.NEXT_PUBLIC_PANEL}accounting/orders`;
export const MOBILE_REGEX_IRAN = /^[0]\d{10}$/;
export const MOBILE_REGEX = /^(\+989|09)\d{9}$/;
export const email = 'info@tahlilgard.com';
export const tell = '26708276-021';
export const IS_SERVER = typeof window === 'undefined';
