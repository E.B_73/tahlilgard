import { decode, JwtPayload } from 'jsonwebtoken';

export default function checkExpireToken(token: string) {
  const decoded = decode(token) as JwtPayload;
  if (decoded && decoded.exp) {
    return decoded?.exp! < Date.now() / 1000;
  } else {
    return true;
  }
}
