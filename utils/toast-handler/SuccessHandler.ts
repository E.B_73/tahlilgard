import { message } from 'antd';
const success = (msg: string, duration: number = 2) => {
  message.success(msg, duration);
};
export default success;
