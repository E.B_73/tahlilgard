import { message } from 'antd';
const error = (error: string) => {
  message.error(error);
};
export default error;
