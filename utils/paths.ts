const paths = {
  home: '/',
  analysis_forum: '/analysis-forum',
  check_auth: '/check-auth',
  explore: '/explore',
  forget_password: '/forget-password',
  learning: '/learning',
  rules: '/rules',
  live_trade: '/live-trade',
  provider: '/provider',
  providerId(id: string) {
    return `/provider/${id}`;
  },
  courses: '/courses',
  courseId(id: string) {
    return `/courses/${id}`;
  },
};

export default paths;
