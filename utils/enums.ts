//enums
export enum AuthStpEnum {
  ENTER_MOBILE = 'enter_mobile',
  LOGIN = 'login',
  REGISTER = 'register',
  SEND_CODE = 'send_code',
}
export enum ForgetPassStpEnum {
  START = 'neutral',
  NEW_PASS = 'send_new_pass',
  SEND_CODE = 'send_code',
}
export enum CardTypeEnum {
  analysis = 'analysis',
  provider = 'provider',
}
