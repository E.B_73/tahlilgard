import { MOBILE_REGEX_IRAN } from '@/utils/constants';

export function checkPhoneNumber(phone: string) {
  if (MOBILE_REGEX_IRAN.test(phone)) {
    return phone.replace(/^[0]/, '+98');
  } else return phone;
}
export function calculateExpirationTime(endDate: number) {
  return Math.floor((endDate - Date.now()) / 1000);
}
